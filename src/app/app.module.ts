import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import { NgRedux, NgReduxModule } from '@angular-redux/store';
import rootReducer from './store';
import { IAppState } from './store/state/IAppState';
import INITIAL_STATE from './store/state';

import {AppComponent} from './app.component';
import {ThemeModule} from './theme/theme.module';
import {AppRoutingModule} from './app-routing.module';
import {HomeComponent} from './platform/home/home.component';
import {CoreModule} from './core/core.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {APIInterceptor} from './APIInterceptor';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthComponent} from './platform/auth/auth.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AuthComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    NgReduxModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: APIInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor (ngRedux: NgRedux<IAppState>) {
    ngRedux.configureStore(rootReducer, INITIAL_STATE);
  }
}
