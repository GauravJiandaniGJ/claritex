export interface MenuItem {
  title: string;
  link?: string;
  url?: string;
  icon?: string;
  expanded?: boolean;
  children?: MenuItem[];
  target?: string;
  hidden?: boolean;
  pathMatch?: string;
  home?: boolean;
  group?: boolean;
  parent?: MenuItem;
  selected?: boolean;
  data?: any;
  fragment?: string;
}
