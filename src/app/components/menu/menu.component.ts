import {Component, Input, OnInit} from '@angular/core';
import {MenuItem} from './MenuItem';
import {Router} from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {

  @Input() public items: MenuItem[];

  @Input() public showOnlyIcons = false;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  validateLink(link = '#') {
    return link !== null && link.length && (link !== '#');
  }

  hasActiveChildren(item: MenuItem) {
    if (!item.children) {
      return false;
    }
    return item.children.some(c => this.router.isActive(c.link, false))
      || this.router.isActive(item.link, false);
  }

  hasActiveChildrenClass(item: MenuItem) {
    return this.hasActiveChildren(item) ? 'active active-child' : '';
  }

  isCollapsed(item: MenuItem) {
    return !item.expanded;
  }

  toggle(item: MenuItem) {
    if (item.children.length) {
      this.router.navigate([item.link]);
    }
    item.expanded = !item.expanded;
  }
}
