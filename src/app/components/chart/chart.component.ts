import {Component, Input, OnInit, ViewChild} from '@angular/core';
import merge from 'deepmerge';
import {ChartType} from './ChartType';

@Component({
  selector: 'app-base-chart',
  templateUrl: './chart.component.html',
})
export class ChartComponent implements OnInit {

  /**
   * Body Element
   */
  static bodyElement: any;

  /**
   * Chart.
   */
  @ViewChild('chart') public chart;

  /**
   * Chart Title.
   */
  @Input() public title: string;

  /**
   * Allowed Chart Types
   */
  @Input() public allowedTypes: ChartType[] = [];

  /**
   * Data Mapper Function.
   */
  @Input() protected dataMapper: Function;

  /**
   * View chart is Full-screen.
   * @type {boolean}
   */
  @Input()
  public isFullScreen = false;

  /**
   * (Current) Chart Type.
   *
   * !! [Use this in the template.] !!
   */
  public type: ChartType;

  /**
   * Chart Data.
   *
   * !! [Use this in the template.] !!
   */
  public chartData: any;

  /**
   * Chart Options.
   *
   * !! [Use this in the template.] !!
   */
  public chartOptions: any = {};

  /**
   * Current Chart Type ID.
   *
   * !! [Use this in the template.] !!
   */
  public currentTypeID: string;

  /**
   * The URL to Download the Chart.
   */
  public downloadURL: string;

  /**
   * Original Options.
   *
   * Stored in order to be used to render
   * the chart when switching between charts.
   * Thus, the chart type options are merged with
   * the original options so that previously active
   * chart's options are not colluded with the new chart.
   */
  private originalOptions: any = {};

  constructor() {
    ChartComponent.bodyElement = document.getElementsByTagName('body')[0];
  }

  ngOnInit(): void {
    // Set to default
    this.switchChartType('');
    this.processOptions(merge({...this.originalOptions}, {...this.type.options}));
  }

  /**
   * Chart Data from Parent Component.
   * @param data
   */
  @Input()
  set data(data: any) {
    this.processData(data);
  }

  /**
   * Chart Options from Parent Component.
   * @param options
   */
  @Input()
  set options(options: any) {
    if (!options) {
      return;
    }
    this.originalOptions = merge({...this.originalOptions}, options);
    let newOptions = options;
    if (this.type && this.type.options) {
      newOptions = merge({...this.type.options}, {...options});
    }
    this.processOptions(newOptions);
  }

  /**
   * Default Chart Type (ID) from Parent Component.
   * @param {string} id
   */
  @Input()
  set defaultTypeID(id: string) {
    this.switchChartType(id);
  }

  get filename() {
    const title = this.title.length ? this.title : this.currentTypeID;
    return `${title}.jpg`;
  }

  /**
   * Change the Chart Type by ID.
   * @param {string} id
   */
  changeChartType(id: string) {
    this.switchChartType(id, true);
  }

  toggleFullScreen() {
    this.isFullScreen = !this.isFullScreen;

    if (this.isFullScreen) {
      ChartComponent.bodyElement.classList.add('is-fullscreen');
    } else {
      ChartComponent.bodyElement.classList.remove('is-fullscreen');
    }
  }

  download(event) {
    const canvas = this.chart.canvas;
    const context = canvas.getContext('2d');
    const image = this.canvasToImage(canvas, context, '#FFF');
    this.downloadURL = image;
    // this.downloadURL = this.chart.canvas.toDataURL();
  }

  canvasToImage(canvas, context, backgroundColor) {
    // cache height and width
    const w = canvas.width;
    const h = canvas.height;
    const y = canvas.height - 85;

    let imgData;
    let compositeOperation = '';
    if (backgroundColor) {

      // context.lineWidth = 1;
      // context.fillStyle = '#000';
      // context.fillText(this.title, 0, y);

      // get the current ImageData for the canvas.
      imgData = context.getImageData(0, 0, w, h);
      // store the current globalCompositeOperation
      compositeOperation = context.globalCompositeOperation;
      // set to draw behind current content
      context.globalCompositeOperation = 'destination-over';
      // set background color
      context.fillStyle = backgroundColor;
      // draw background / rect on entire canvas
      context.fillRect(0, 0, w, h);
    }
    // get the image data from the canvas
    const imageData = canvas.toDataURL('image/png');
    if (backgroundColor) {
      // clear the canvas
      context.clearRect (0, 0, w, h);
      // restore it with original / cached ImageData
      context.putImageData(imgData, 0, 0);
      // reset the globalCompositeOperation to what it was
      context.globalCompositeOperation = compositeOperation;
    }
    // return the Base64 encoded data url string
    return imageData;
  }

  protected processData(data: any) {
    if (this.dataMapper) {
      this.chartData = this.dataMapper(data);
    } else {
      this.chartData = data;
    }
  }

  protected processOptions(options: any) {
    if (options) {
      this.chartOptions = options;
    }
  }

  /**
   * Switch the Chart Type by ID.
   * @param {string} id
   * @param {boolean} reRender
   */
  private switchChartType(id: string, reRender: boolean = false) {
    if (!id.length) {
      id = this.currentTypeID;
    }

    let chartType = this.allowedTypes.find(t => t.id === id);

    if (!chartType) {
      chartType = this.allowedTypes[0];
    }

    this.type = chartType;
    this.currentTypeID = chartType.id;

    if (reRender) {
      this.processData(this.chartData);
      this.processOptions(merge({...this.type.options}, {...this.originalOptions}));
    }

  }
}
