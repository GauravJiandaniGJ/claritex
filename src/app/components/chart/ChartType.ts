export class ChartType {

  constructor(public id: string, public name: string, public slug: string, public icon: string, public options = {}) {
    //
  }
}
