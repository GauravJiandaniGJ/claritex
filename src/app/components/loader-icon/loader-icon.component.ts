import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-loader-icon',
  styleUrls: ['./loader-icon.component.scss'],
  template: '<div class="loader">Loading...</div>'
})
export class LoaderIconComponent {

  @Input()
  color = 'rgba(0,0,0,0.3)';

  @Input()
  size = 48;

}
