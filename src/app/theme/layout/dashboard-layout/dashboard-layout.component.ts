import {Component, OnInit} from '@angular/core';
import {SchemeService} from '../../../core/services/scheme.service';
import {PensionScheme} from '../../../core/models/PensionScheme';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../../store/state/IAppState';
import {User} from '../../../core/models/User';
import {UserService} from '../../../core/services/user.service';
import {AuthService} from '../../../core/services/auth.service';

@Component({
  selector: 'app-dashboard-layout',
  templateUrl: './dashboard-layout.component.html',
  styleUrls: ['./dashboard-layout.component.scss']
})
export class DashboardLayoutComponent implements OnInit {

  currentScheme: PensionScheme;

  currentUser: User;

  costTreeTemplate: any = 'IDWG';

  // popoverVisible = false;

  pensionSchemes: PensionScheme[];

  // @ViewChild('costTreePopover') costTreePopover;

  isCollapsed = true;

  sidebarIsCollapsed = true;

  body: any;

  constructor(private schemeService: SchemeService, private router: Router, private route: ActivatedRoute,
              private ngRedux: NgRedux<IAppState>, private userService: UserService, private authService: AuthService) {
    this.body = document.querySelector('body');
  }

  ngOnInit() {
    this.fetchPensionSchemes();
    this.subscribeToRouteChange();
    this.userService.getCurrentUser().then((user: User) => {
      this.currentUser = user;
    }).catch(e => this.logout());
  }

  private fetchPensionSchemes() {
    this.ngRedux.select<IAppState[]>('scheme').subscribe((result: any) => {
      if (result.scheme !== undefined) {
        this.pensionSchemes = result.scheme.pensionScheme;
      } else {
        this.pensionSchemes = [];
      }
      this.resolveCurrentScheme.bind(this);
    });
  }

  switchDashboard(event) {
    const scheme = event.value;
    this.router.navigate([`/dashboard/scheme/${scheme.id}`]);
  }

// showCostTree(content) {
//   this.modalService.open(content, {size: 'lg'}).result.then((result) => {
//     //
//   });
// }

  private subscribeToRouteChange() {
    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.resolveCurrentScheme();
      }
    });
  }

  private resolveCurrentScheme() {
    const id = this.route.firstChild.snapshot.paramMap.get('id');
    if (id) {
      const scheme_id = parseInt(id, 10);
      this.currentScheme = this.pensionSchemes.find(s => s.id === scheme_id);
      return;
    }

    this.currentScheme = null;

  }

  logout() {
    this.authService.logout().then(() => this.router.navigate(['/']));
  }

  toggleSidebar(event) {
    event.stopPropagation();
    this.body.classList.toggle('sidebar-visible');
  }
}
