const basePalette = {
  primary: '#154E65',
  success: '#F0AB02',
  info: '#4ca6ff',
  warning: '#F0AB02',
  danger: '#ff4c6a',
};

const palette = {
  ...basePalette,
  primaryLight: basePalette.primary,
  successLight: basePalette.success,
  infoLight: basePalette.info,
  warningLight: basePalette.warning,
  dangerLight: basePalette.danger,
};

export const THEME_CONFIG = {
  name: 'default',
  base: null,
  variables: {
    fontMain: 'Roboto, "Helvetica Neue", Arial, sans-serif',
    fontSecondary: 'Roboto, "Helvetica Neue", Arial, sans-serif',
    bg: '#ffffff',
    fg: '#a4abb3',
    fgHeading: '#2a2a2a',
    fgText: '#3b3b3b',
    fgHighlight: '#41d974',
    layoutBg: '#ebeff5',
    separator: '#ebeef2',
    primary: palette.primary,
    success: palette.success,
    info: palette.info,
    warning: palette.warning,
    danger: palette.danger,
    primaryLight: palette.primaryLight,
    successLight: palette.successLight,
    infoLight: palette.infoLight,
    warningLight: palette.warningLight,
    dangerLight: palette.dangerLight,

    chartjs: {
      axisLineColor: '#cccccc',
      textColor: '#484848',
    },
  },
};
