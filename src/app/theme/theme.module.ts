import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SidebarLayoutComponent} from './layout/sidebar-layout/sidebar-layout.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {DashboardLayoutComponent} from './layout/dashboard-layout/dashboard-layout.component';
import {RouterModule} from '@angular/router';
import {DropdownModule, MultiSelectModule, OrganizationChartModule} from 'primeng/primeng';
import {LoaderIconComponent} from '../components/loader-icon/loader-icon.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {MenuComponent} from '../components/menu/menu.component';

const BASE_MODULES = [
  CommonModule,
  FormsModule,
  RouterModule,
  ReactiveFormsModule,
];

const VENDOR_MODULES = [
  NgbModule,
  MultiSelectModule,
  DropdownModule,
  OrganizationChartModule,
];

const COMPONENTS = [
  SidebarLayoutComponent,
  DashboardLayoutComponent,
  LoaderIconComponent,
  SidebarComponent
];

@NgModule({
  imports: [
    ...BASE_MODULES,
    ...VENDOR_MODULES,
  ],
  exports: [
    ...BASE_MODULES,
    ...VENDOR_MODULES,
    ...COMPONENTS,
  ],
  declarations: [
    SidebarLayoutComponent,
    DashboardLayoutComponent,
    LoaderIconComponent,
    SidebarComponent,
    MenuComponent,
  ]
})
export class ThemeModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders> {
      ngModule: ThemeModule,
      providers: [],
    };
  }
}
