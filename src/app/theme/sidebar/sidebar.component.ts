import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {MenuItem} from '../../components/menu/MenuItem';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, NavigationEnd, NavigationStart, Router, RouterEvent} from '@angular/router';
import {TreeNode} from 'primeng/api';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit, AfterViewInit {
  @Input() public items: MenuItem[];

  @Input() public mini = false;
  costTreeTemplate: any = 'IA';

  popoverVisible = false;
  private body: any;
  data: TreeNode[];
  org_data: any[];
  tree_index = 0;
  @ViewChild('costTreePopover') costTreePopover;

  constructor(private modalService: NgbModal, private router: Router) {
  }

  ngOnInit() {
    this.body = document.querySelector('body');
    this.data = [{
      label: 'Total Cost',
      expanded: true,
      children: []
    }];

    this.org_data = [{
      label: 'Total Cost',
      expanded: true,
      children: [
        {
          label: 'Administrative Costs',
          expanded: true,
          children: [
            {label: 'Executive Costs'},
            {label: 'Office Costs'},
            {label: 'Advisory Costs'},
            {label: 'Governance Costs'},
            {label: 'Member Administrations Costs'},
            {label: 'Miscellaneous Costs'}
          ]
        },
        {
          label: 'Investment Costs',
          expanded: true,
          children: [
            {
              label: 'Investment Management Costs',
              expanded: true,
              children: [
                {label: 'Management Fees'},
                {label: 'Performance Fees'},
                {label: 'Transaction Fees'},
                {label: 'Research Payment Accounts'}
              ]
            },
            {
              label: 'Auxiliary Services Costs',
              expanded: true,
              children: [
                {label: 'Investment Consultant Costs'},
                {label: 'Custodian Costs'},
                {label: 'Performance Measurement Costs'},
                {label: 'Investment Committee Costs'},
                {label: 'Miscellaneous Costs'}
              ]
            },
          ]
        }
      ]
    }];
  }

  ngAfterViewInit(): void {
    this.body.classList.add('page-has-sidebar');

    document.addEventListener('click', (e) => {
      this.body.classList.remove('sidebar-visible');
    });

    setTimeout(() => this.openPopover(), 2000);
  }

  sidebarClick(event) {
    event.stopPropagation();
  }

  showCostTree(content) {
    this.modalService.open(content, {size: 'lg'}).result.then((result) => {
      //
    });
  }

  openPopover() {
    if (this.router.isActive('/dashboard', true)) {
      setTimeout(() => this.popoverVisible = true, 3000); // Buffer time
      this.costTreePopover.open();
    }
  }

  filterItems(data) {
    let abc = {};
    return abc = this.org_data.reduce((total, child) => {
      this.filterItems(child);
      if (data.label === child.label) {
        return total = child;
      }
    }, {});
  }


  onNodeSelect(event) {
    // const fin_data = this.filterItems(event.node);
    // console.log(fin_data);
    this.data[this.tree_index].children.push(
      {
        label: 'Administrative Costs',
        expanded: true,
        children: []
      },
      {
        label: 'Investment Costs',
        expanded: false,
        children: []
      }
    );
    this.tree_index++;
  }

  hidePopover() {
    if (this.popoverVisible) {
      this.costTreePopover.close();
    }
  }
}
