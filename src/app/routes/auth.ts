import {Routes} from '@angular/router';
import {AuthComponent} from '../platform/auth/auth.component';

export const AUTH_ROUTES: Routes = [
  {
    path: 'auth',
    component: AuthComponent,
  },
];
