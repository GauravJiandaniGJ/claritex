import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {PensionScheme} from '../../core/models/PensionScheme';
import {SchemeService} from '../../core/services/scheme.service';
import {Injectable} from '@angular/core';

@Injectable()
export class PensionSchemeResolver implements Resolve<PensionScheme> {

  constructor(private pensionSchemeService: SchemeService, private router: Router) {
    //
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<PensionScheme> {
    const id = route.paramMap.get('id');

    return this.pensionSchemeService.get(id).then(scheme => {
      if (scheme) {
        return scheme;
      }

      // noinspection JSIgnoredPromiseFromCall
      this.router.navigate(['/dashboard']);
      return null;
    });
  }
}
