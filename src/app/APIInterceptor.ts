import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../environments/environment';

@Injectable()
export class APIInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url === 'http://www.mocky.io/v2/5ad99c7f2f00005400cfde02') {
      const apiReq1 = req.clone({url: `${req.url}`});
      return next.handle(apiReq1);
    }
    const authToken = window.localStorage.getItem('token');
    const apiReq = req.clone({
      url: `${environment.apiUrl}/${req.url}`,
      headers: req.headers.set('Authorization', `Bearer ${authToken}`),
    });
    return next.handle(apiReq);
  }

}
