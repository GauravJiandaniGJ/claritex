export const GET_PENSION_SCHEME = 'GET_PENSION_SCHEME';
export const ADD_PENSION_SCHEME = 'ADD_PENSION_SCHEME';
export const GET_ALL_PENSION_SCHEME = 'GET_ALL_PENSION_SCHEME';
export const REMOVE_ALL_PENSION_SCHEME = 'REMOVE_ALL_PENSION_SCHEME';
export const ADD_ALL_PENSION_SCHEME = 'ADD_ALL_PENSION_SCHEME';

// import fetch from 'cross-fetch';

// export const REQUEST_SCHEMES = 'REQUEST_SCHEMES';
// function requestSchemes(subreddit) {
//   return {
//     type: REQUEST_SCHEMES,
//     subreddit
//   };
// }

// export const RECEIVE_SCHEMES = 'RECEIVE_SCHEMES';
// function receiveSchemes(subreddit, json) {
//   return {
//     type: RECEIVE_SCHEMES,
//     subreddit,
//     posts: json.data.children.map(child => child.data),
//     receivedAt: Date.now()
//   };
// }

// function fetchSchemes(subreddit) {
//     return dispatch => {
//       dispatch(requestSchemes(subreddit));
//       return fetch(`http://www.mocky.io/v2/5ad5ef543200006e00202dd4/pension-schemes/1`)
//         .then(response => response.json())
//         .then(json => dispatch(receiveSchemes(subreddit, json)));
//     };
// }
