import {combineReducers, Reducer} from 'redux';
import reducers from './reducers';
import { IAppState } from './state/IAppState';
const allReducers: Reducer<IAppState> = combineReducers(reducers);
export default allReducers;


