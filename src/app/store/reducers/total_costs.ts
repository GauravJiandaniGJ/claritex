import { ADD_PENSION_SCHEME_TOTAL_COST } from './../actions';
import { IAppState } from './../state/IAppState';

export  default function(state = {}, action) {
    switch (action.type) {
        case ADD_PENSION_SCHEME_TOTAL_COST:
            const total_costs = {
                pensionSchemeTotalCosts: action.total_costs.pensionSchemeTotalCosts,
                lastUpdate: new Date()
            };
            return Object.assign({}, state , {
                total_costs: total_costs
            });
        default:
            return state;
    }

}



