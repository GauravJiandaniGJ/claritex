import { ADD_ADMIN_COST_AND_TYPE } from './../actions';
import { IAppState } from './../state/IAppState';

export  default function(state = {}, action) {
    switch (action.type) {
        case ADD_ADMIN_COST_AND_TYPE:
            const admin_cost_and_type = {
                adminCostAndType: action.admin_cost_and_type.adminCostAndType,
                lastUpdate: new Date()
            };
            return Object.assign({}, state , {
                admin_cost_and_type: admin_cost_and_type
            });
        default:
            return state;
    }

}
