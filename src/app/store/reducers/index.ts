import auth from './auth';
import scheme from './scheme';
import total_costs from './total_costs';
import universe_total_costs_net_assets from './universe_total_costs_net_assets';
import admin_cost_and_type from './admin_cost_and_type';

export default {
  scheme,
  auth,
  total_costs,
  universe_total_costs_net_assets,
  admin_cost_and_type
};
