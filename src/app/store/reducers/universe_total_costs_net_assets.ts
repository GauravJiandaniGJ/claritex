import { ADD_UNIVERSE_TOTAL_COSTS_NET_ASSET } from './../actions';
import { IAppState } from './../state/IAppState';

export  default function(state = {}, action) {
    switch (action.type) {
        case ADD_UNIVERSE_TOTAL_COSTS_NET_ASSET:
            const universe_total_costs_net_assets = {
                universeTotalCostAndNetAsset: action.universe_total_costs_net_assets.universeTotalCostAndNetAsset,
                lastUpdate: new Date()
            };
            return Object.assign({}, state , {
                universe_total_costs_net_assets: universe_total_costs_net_assets
            });
        default:
            return state;
    }

}



