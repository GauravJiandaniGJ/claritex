import {USER_LOGGED_IN} from '../actions';

export default function (state = {}, action) {
  switch (action.type) {
    case USER_LOGGED_IN:
      const auth = {
        user: action.auth.user,
        lastUpdate: new Date()
      };
      return Object.assign({}, state, {
        auth: auth
      });
    default:
      return state;
  }

}



