import {ADD_ALL_PENSION_SCHEME} from '../actions';

export default function (state = {}, action) {
  switch (action.type) {
    case ADD_ALL_PENSION_SCHEME:
      const scheme = {
        pensionScheme: action.scheme.pensionScheme,
        lastUpdate: new Date()
      };
      return Object.assign({}, state, {
        scheme: scheme
      });
    default:
      return state;
  }

}



