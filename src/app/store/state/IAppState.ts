import {PensionScheme} from './../../core/models/PensionScheme';
import {PensionSchemeTotalCosts} from './../../core/models/PensionSchemeTotalCosts';
import {UniverseTotalCostAndNetAsset} from './../../core/models/UniverseTotalCostAndNetAsset';
import {AdminCostAndType} from './../../core/models/AdminCostAndType';
import {User} from '../../core/models/User';

export interface IAppState {
  scheme: {
    pensionScheme: PensionScheme[];
    lastUpdate: Date;
  };
  total_costs: {
    pensionSchemeTotalCosts: PensionSchemeTotalCosts[];
    lastUpdate: Date;
  };
  universe_total_costs_net_assets: {
    universeTotalCostAndNetAsset: UniverseTotalCostAndNetAsset[];
    lastUpdate: Date;
  };
  admin_cost_and_type: {
    adminCostAndType: AdminCostAndType[];
    lastUpdate: Date;
  };
  auth: {
    user: User;
    lastUpdate: Date;
  };
}
