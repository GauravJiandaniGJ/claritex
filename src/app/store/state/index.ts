import schemeState from './scheme';
import totalCostState from './total_costs';
import universeTotalCostAndNetAsset from './universe_total_costs_net_assets';
import adminCostAndType from './admin_cost_and_type';
import authState from './auth';

import { IAppState } from './IAppState';


const state = {
  scheme: {
    lastUpdate: null,
    ...schemeState,
  },
  auth: {
    lastUpdate: null,
    ...authState,
  },
  total_costs: {
    lastUpdate: null,
    ...totalCostState
  },
  universe_total_costs_net_assets: {
    lastUpdate: null,
    ...universeTotalCostAndNetAsset
  },
  admin_cost_and_type: {
    lastUpdate: null,
    ...adminCostAndType
  }
};

export default state;
