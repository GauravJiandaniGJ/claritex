import merge from 'deepmerge';
import * as randomColor from 'randomcolor';

import * as numeral from 'numeral';

export class Utils {

  public static SYMBOL = '£';

  public static formatNumber(n: number, format = '0a') {
    return numeral(n).format(format);
  }

  public static humanizeNum(n: number, isWhole = false) {
    const mv = n;
    return Utils.humanize(mv, isWhole);
  }

  public static humanize(mv: number, isWhole = false) {
    const frmt = isWhole ? '0,0' : '0.[0]a';
    return this.SYMBOL + numeral(mv).format(frmt);
  }

  public static wholeNum(mv: number) {
    return Utils.humanize(mv, true);
  }

  public static convertRange(number, oldMin, oldMax, newMin, newMax) {
    return (((newMax - newMin) * (number - oldMin)) / (oldMax - oldMin)) + newMin;
  }

  public static calculateBPS(cost: number, net: number) {
    return Math.fround((cost / net) * 10000);
  }

  public static throttle(callback, limit) {
    let wait = false;
    return () => {
      if (!wait) {
        callback.call();
        wait = true;
        setTimeout(() => {
          wait = false;
        }, limit);
      }
    };
  }

  public static randomColor(options: {} | number = {}) {

    const colors = [
      '#838b8b',
      'rgb(17,77, 100)',
      'rgb(224, 173, 48)',
      '#008b8b',
      '#2f4f4f',
      '#8b8970',
      'rgb(2, 50, 80)',
      'rgb(160, 170, 183)',
      '#1e90ff',
      '#528b8b',
      '#8b7500',
      '#8b8378',
      '#9bcd9b',
      '#b8860b',
      '#20b2aa',
      'rgb(106, 125, 144)',
    ];

    if (typeof options === 'number') {
      options = {
        seed: options,
      };
    }

    if (options.hasOwnProperty('count') && colors.length > options['count']) {
      return colors.slice(0, options['count']);
    }

    options = merge({
      format: 'rgba',
      alpha: '0.3',
      luminosity: 'dark',
    }, options);

    return randomColor(options);
  }

  public static symbolForUnit(unit = 'gpb') {
    if (unit === 'gbp') {
      return '£';
    }

    if (unit === 'bps') {
      return ' BPS';
    }

    if (unit === 'percent') {
      return '%';
    }

    return '';
  }

  public static labelForUnit(label: number, unit: string, format = '0a') {
    label = Utils.formatNumber(label, format);

    if (unit === 'gbp') {
      return Utils.symbolForUnit(unit) + label;
    }

    return label + Utils.symbolForUnit(unit);
  }

  public static defaultFormatForUnit(unit: string) {
    if (unit === 'gbp') {
      return '0,0';
    }

    return '0a';
  }

}
