import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../core/services/auth.service';
import {AccessToken} from '../../core/models/auth/AccessToken';
import {User} from '../../core/models/User';

@Component({
  selector: 'app-home',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  error = false;
  public email = '';
  public password = '';

  constructor(private router: Router, private auth: AuthService) {
    //

  }

  ngOnInit() {
  }

  login(event) {
    event.preventDefault();

    if (!this.email && this.password) {
      return false;
    }

    const email = this.email.toLowerCase();

    this.auth.authenticate(email, this.password).then((accessToken: AccessToken) => {
      this.error = false;
      window.localStorage.setItem('token', accessToken.token);
      this.router.navigate(['/dashboard']);
    }).catch(e => {
      this.error = true;
    });

    return false;
  }

}
