import {Component, Input, OnInit} from '@angular/core';
import {ChartTypesService} from '../../core/services/chart-types.service';
import merge from 'deepmerge';
import {ChartComponent} from '../../components/chart/chart.component';
import {Utils} from '../../utils/Utils';

@Component({
  selector: 'app-breakdown-chart',
  templateUrl: '../../components/chart/chart.component.html',
})
export class BreakdownChartComponent extends ChartComponent implements OnInit {

  static DEFAULT_CHART_TYPE = 'pie';

  @Input()
  currentUnit = '';

  @Input()
  xAxisLabel = '';

  @Input()
  yAxisLabel = 'Costs';

  constructor(private chartTypesService: ChartTypesService) {
    super();
    BreakdownChartComponent.DEFAULT_CHART_TYPE = chartTypesService.breakdownChartTypes()
      .find((ct, i) => i === 0).id;
  }

  static defaultOptions() {
    return {
      legend: {
        position: 'top',
      },
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            const label = data.labels[tooltipItem.index];
            const dataset = data.datasets[tooltipItem.datasetIndex];
            const formattedValue = dataset.formatted[tooltipItem.index];
            return `${label}: ${formattedValue}`;
          }
        }
      },
    };
  }

  ngOnInit(): void {
    this.options = merge({...BreakdownChartComponent.defaultOptions()}, {...this.options});
    this.allowedTypes = this.chartTypesService.breakdownChartTypes();
    this.defaultTypeID = BreakdownChartComponent.DEFAULT_CHART_TYPE;
    super.ngOnInit();
  }

  processOptions(options = {}) {
    if (this.currentTypeID === 'bar') {
      options['scales'] = {
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: this.yAxisLabel,
          },
          ticks: {
            autoSkip: false,
            beginAtZero: true,
            callback: (label) => Utils.labelForUnit(label, this.currentUnit),
          }
        }],
        xAxes: [{
          ticks: {
            min: 0,
            autoSkip: false,
            beginAtZero: true,
          },
        }],
      };
    }
    super.processOptions(options);
  }

}
