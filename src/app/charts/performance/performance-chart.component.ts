import {Component, Input, OnInit} from '@angular/core';
import {ChartTypesService} from '../../core/services/chart-types.service';
import merge from 'deepmerge';
import {ChartComponent} from '../../components/chart/chart.component';
import {Utils} from '../../utils/Utils';

@Component({
  selector: 'app-performance-chart',
  templateUrl: '../../components/chart/chart.component.html',
})
export class PerformanceChartComponent extends ChartComponent implements OnInit {

  static DEFAULT_CHART_TYPE = 'scatter';

  @Input()
  currentUnit = '';

  @Input()
  labelX: string;

  @Input()
  labelY: string;

  constructor(private chartTypesService: ChartTypesService) {
    super();
  }

  private defaultOptions(labels: { x: string, y: string }) {
    const datasetRatios = this.chartData.datasets.map(d => d.data[0].y / d.data[0].x);
    const maxDatasetValue = Math.max(...datasetRatios);
    return {
      legend: {
        display: false
      },
      elements: {
        point: {
          radius: (context) => {
            const index = context.dataIndex;
            const data = context.dataset.data[index];
            const chartSize = (context.chart.width / 200) < 6 ? 6 : context.chart.width / 200;
            const base = ((maxDatasetValue - (data.y / data.x)) * chartSize);
            const max = ((maxDatasetValue) * chartSize);
            const radius = (Utils.convertRange(base, 0, max, 1, 15));
            return chartSize + radius;
          },
        }
      },
      tooltips: {
        callbacks: {
          title: (tooltipItem, data) => {
            const dataset = data.datasets[tooltipItem[0].datasetIndex];
            return dataset.title;
          },
          label: (tooltipItem, data) => {
            const dataset = data.datasets[tooltipItem.datasetIndex];
            return dataset.label;
          },
          footer: (tooltipItem, data) => {
            const dataset = data.datasets[tooltipItem[0].datasetIndex];
            return dataset.footer;
          },
        }
      },
    };
  }

  processOptions(options = {}) {
    options['scales'] = {
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: this.labelY,
        },
        ticks: {
          autoSkip: false,
          beginAtZero: true,
          callback: (label) => Utils.labelForUnit(label, 'percent', '0.00a'),
        }
      }],
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: this.labelX,
        },
        ticks: {
          min: 0,
          callback: (label) => Utils.labelForUnit(label, this.currentUnit, '0.0a'),
        },
      }],
    };

    super.processOptions(options);
  }

  ngOnInit(): void {
    const labels = {x: this.labelX, y: this.labelY};
    const options = merge({...this.defaultOptions(labels)}, {...this.options});
    this.options = Object.assign({}, options);
    this.allowedTypes = this.chartTypesService.universeComparisionChartTypes();
    this.defaultTypeID = PerformanceChartComponent.DEFAULT_CHART_TYPE;
    super.ngOnInit();
  }
}
