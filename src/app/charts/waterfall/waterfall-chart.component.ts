import {Component, Input, OnInit} from '@angular/core';
import {ChartTypesService} from '../../core/services/chart-types.service';
import merge from 'deepmerge';
import {ChartComponent} from '../../components/chart/chart.component';

@Component({
  selector: 'app-waterfall-chart',
  templateUrl: '../../components/chart/chart.component.html',
})
export class WaterfallChartComponent extends ChartComponent implements OnInit {

  static DEFAULT_CHART_TYPE = 'waterfall';

  @Input()
  currentUnit = '';

  @Input()
  xAxisLabel = '';

  @Input()
  yAxisLabel = 'Costs';

  constructor(private chartTypesService: ChartTypesService) {
    super();
    WaterfallChartComponent.DEFAULT_CHART_TYPE = chartTypesService.waterfallChartTypes()
      .find((ct, i) => i === 0).id;
  }

  static defaultOptions() {
    return {
      scaleOverride: true, scaleSteps: 3, scaleBeginAtZero: false,
      scales: {
        xAxes: [{
          stacked: false,
          beginAtZero: true,
          ticks: {
            autoSkip: false
          }
        }]
      },
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            const label = data.labels[tooltipItem.index];
            const dataset = data.datasets[tooltipItem.datasetIndex];
            const value = dataset.data[tooltipItem.index];

            if (tooltipItem.index > 0) {
              const prevValue = dataset.data[--tooltipItem.index];
              const diff = value - prevValue;
              return `${label}: ${value} (${diff})`;
            }

            return `${label}: ${value}`;
          }
        }
      },
    };
  }


  ngOnInit(): void {
    this.options = merge({...WaterfallChartComponent.defaultOptions()}, {...this.options});
    this.allowedTypes = this.chartTypesService.waterfallChartTypes();
    this.defaultTypeID = WaterfallChartComponent.DEFAULT_CHART_TYPE;
    super.ngOnInit();
  }

  processOptions(options = {}) {
    super.processOptions(options);
  }

  filterDummyStacks = (legendItem, chartData) => {
    const currentDataset = chartData.datasets[legendItem.datasetIndex];
    return !currentDataset.waterfall.dummyStack;
  }

}
