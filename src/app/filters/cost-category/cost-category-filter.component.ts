import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-cost-category-filter',
  templateUrl: './cost-category-filter.component.html',
})
export class CostCategoryFilterComponent implements OnInit {

  @Input()
  labelKey = 'name';

  @Input()
  dataKey = 'id';

  @Input()
  placeholder = 'Cost Category';

  @Input()
  categories: Array<any>;

  @Input()
  selected: Array<any> = [];

  @Output()
  change = new EventEmitter();

  constructor() {
    //
  }

  ngOnInit(): void {
  }

  onSelect(event) {
    this.change.emit(event);
  }

}
