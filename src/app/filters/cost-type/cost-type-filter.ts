import {Injectable} from '@angular/core';
import {Utils} from '../../utils/Utils';


@Injectable()
export class CostTypeFilter {

  constructor() {
    //
  }

  static generateTitle(label, costTypes: Array<{ amount: number }>, net: number, type = 'bps') {
    const costTypeTotals = costTypes.map(c => c.amount);
    const costTotal = costTypeTotals.reduce((t, c) => t + c);

    if (type === 'percent') {
      return label + ': ' + Math.fround((costTotal / net) * 100) + '%';
    }

    return label + ': ' + Utils.calculateBPS(costTotal, net) + ' BPS';
  }

  forTable(data: any, options: { selectedCostTypes: Array<any>, net_assets: number }): any {
    if (!options.selectedCostTypes.length) {
      return data;
    }

    return data.filter(d => {
      return (options.selectedCostTypes.filter(c => {
        return d.category === c.type;
      })).length > 0;
    });
  }

  forBreakdown(data: any, options: { selectedCostTypes: Array<any>, net_assets: number }): any {
    let filtered = data.original;

    if (options.selectedCostTypes.length) {
      filtered = options.selectedCostTypes;
    }

    return {
      labels: filtered.map(adminCostAndType => adminCostAndType.type),
      original: filtered,
      datasets: [{
        data: [...filtered.map(adminCostAndType => adminCostAndType.amount)],
        formatted: [...filtered.map(adminCostAndType => Utils.humanize(adminCostAndType.amount))],
        backgroundColor: [...data.datasets[0].backgroundColor],
      }],
    };
  }

  forAuxComparision(data: any, options: any) {
    if (!options.selectedCostTypes.length) {
      return data;
    }

    data.datasets = data.datasets.map(ds => {
      ds.original.auxCostAndTypes = ds.original.auxCostAndTypes.filter(cost => {
        return (options.selectedCostTypes.filter(c => {
          return cost.type_id === c.type_id;
        })).length > 0;
      });
      return ds;
    });

    return data;
  }

  forAdminComparision(data: any, options: any) {
    if (!options.selectedCostTypes.length) {
      return data;
    }

    data.datasets = data.datasets.map(ds => {
      ds.original.adminCostAndTypes = ds.original.adminCostAndTypes.filter(cost => {
        return (options.selectedCostTypes.filter(c => {
          return cost.type_id === c.type_id;
        })).length > 0;
      });
      return ds;
    });

    return data;
  }
}
