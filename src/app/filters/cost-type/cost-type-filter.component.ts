import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-cost-type-filter',
  templateUrl: './cost-type-filter.component.html',
})
export class CostTypeFilterComponent implements OnInit {

  @Input()
  labelKey = 'type';

  @Input()
  dataKey = 'type_id';

  @Input()
  placeholder = 'Cost Type';

  @Input()
  costTypes: Array<any>;

  @Input()
  selected: Array<any> = [];

  @Output()
  change = new EventEmitter();

  constructor() {
    //
  }

  ngOnInit(): void {
  }

  onSelect(event) {
    this.change.emit(event);
  }

}
