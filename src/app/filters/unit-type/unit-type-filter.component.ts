import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SelectItem} from 'primeng/primeng';

@Component({
  selector: 'app-unit-type-filter',
  templateUrl: './unit-type-filter.component.html',
})
export class UnitTypeFilterComponent implements OnInit {

  @Input()
  labelKey = 'name';

  @Input()
  placeholder = 'Unit Type';

  @Input()
  options: SelectItem[] = [
    {
      label: '%',
      value: 'percent',
    },
    {
      label: 'BPS',
      value: 'bps',
    },
    {
      label: '£',
      value: 'gbp',
    }
  ];

  @Input()
  selected = 'gbp';

  @Output()
  change = new EventEmitter();

  constructor() {
    //
  }

  ngOnInit(): void {
  }

  onSelect(event) {
    this.change.emit(event);
  }

}
