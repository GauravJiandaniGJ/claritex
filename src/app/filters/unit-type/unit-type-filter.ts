import {Injectable} from '@angular/core';
import {Utils} from '../../utils/Utils';


@Injectable()
export class UnitTypeFilter {

  constructor() {
    //
  }

  static generateTitle(label, costTypes: Array<{ amount: number }>, net: number, type = 'bps') {
    const costTypeTotals = costTypes.map(c => c.amount);
    const costTotal = costTypeTotals.reduce((t, c) => t + c, 0);

    if (type === 'gbp') {
      return label + ': ' + Utils.humanize(costTotal);
    }

    if (type === 'percent') {
      return label + ': ' + Utils.formatNumber(Math.fround((costTotal / net) * 100), '0.0a') + '%';
    }

    return label + ': ' + Utils.formatNumber(Utils.calculateBPS(costTotal, net), '0.0a') + ' BPS';
  }

  forBreakdown(d: any, options: { selectedUnit: string, net_assets: number }): any {
    if (options.selectedUnit === 'bps') {
      d.datasets[0].data = d.datasets[0].data.map(v => Utils.calculateBPS(v, options.net_assets));
      d.datasets[0].formatted = d.datasets[0].formatted.map(
        (v, i) => Utils.formatNumber((d.datasets[0].data[i]), '0.0a') + ' BPS'
      );
    }

    if (options.selectedUnit === 'percent') {
      const total = d.datasets[0].data.reduce((t, v) => t + v);
      d.datasets[0].data = d.datasets[0].data.map(v => Math.fround((v / total) * 100));
      d.datasets[0].formatted = d.datasets[0].formatted.map(
        (v, i) => Utils.formatNumber((d.datasets[0].data[i]), '0.0a') + '%'
      );
    }

    if (options.selectedUnit === 'gbp') {
      d.datasets[0].formatted = d.datasets[0].formatted.map(
        (v, i) => Utils.SYMBOL + Utils.formatNumber((d.datasets[0].data[i]), '0.0a')
      );
    }

    return d;
  }

  forComparision(d: any, options: { selectedUnit: string }): any {
    if (options.selectedUnit === 'gbp') {
      d.datasets = d.datasets.map(ds => {
        ds.label[0] = `${ds.label[0]}`.substr(0, ds.label[0].indexOf(':')) + ': ' + Utils.humanizeNum(ds.data[0].y) + '';
        return ds;
      });
    }

    if (options.selectedUnit === 'bps') {
      d.datasets = d.datasets.map(ds => {
        ds.data[0].y = Utils.calculateBPS(ds.data[0].y, ds.data[0].x);
        ds.label[0] = `${ds.label[0]}`.substr(0, ds.label[0].indexOf(':')) +
          ': ' + Utils.formatNumber((ds.data[0].y), '0.0a') + ' BPS';
        return ds;
      });
    }

    if (options.selectedUnit === 'percent') {
      d.datasets = d.datasets.map(ds => {
        ds.data[0].y = Utils.calculateBPS(ds.data[0].y, ds.data[0].x);
        return ds;
      });

      const total = d.datasets.reduce((t, c) => t + c.data[0].y, 0);
      d.datasets = d.datasets.map(ds => {
        ds.data[0].y = Math.fround((ds.data[0].y / total) * 100);
        ds.label[0] = `${ds.label[0]}`.substr(0, ds.label[0].indexOf(':')) +
          ': ' + Utils.formatNumber((ds.data[0].y), '0.0a') + '%';
        return ds;
      });
    }

    return d;
  }

  forTable(d: Array<any>, options: { selectedUnit: string, net_assets: number }): any {
    if (options.selectedUnit === 'bps') {
      d = d.map(r => {
        r.amount = Utils.calculateBPS(r.amount, options.net_assets);
        r.formattedAmount = Utils.formatNumber(r.amount, '0.0a') + ' BPS';
        return r;
      });
    }

    if (options.selectedUnit === 'percent') {
      const total = d.reduce((t, c) => t + c.amount, 0);
      d = d.map(r => {
        r.amount = Math.fround((r.amount / total) * 100);
        r.formattedAmount = Utils.formatNumber(r.amount, '0.0a') + '%';
        return r;
      });
    }

    return d;
  }

  forAuxComparision(d: any, options: { selectedUnit: string, label: string }): any {
    if (options.selectedUnit === 'gbp') {
      d.datasets = d.datasets.map(ds => {
        const costTypeTotals = ds.original.auxCostAndTypes.map(c => c.amount);
        const costTotal = costTypeTotals.reduce((t, c) => t + c, 0);
        ds.data[0] = {
          x: ds.data[0].x,
          y: costTotal,
        };

        ds.title = UnitTypeFilter.generateTitle(options.label, ds.original.auxCostAndTypes, ds.original.net_assets, 'gbp');
        ds.label = ds.original.auxCostAndTypes.map(c => {
          return `${c.type}: ${Utils.humanize(c.amount)}`;
        });

        return ds;
      });
    }

    if (options.selectedUnit === 'bps') {
      d.datasets = d.datasets.map(ds => {
        const costTypeTotals = ds.original.auxCostAndTypes.map(c => c.amount);
        const costTotal = costTypeTotals.reduce((t, c) => t + c, 0);

        ds.data[0] = {
          x: ds.data[0].x,
          y: Utils.calculateBPS(costTotal, ds.data[0].x),
        };

        ds.title = UnitTypeFilter.generateTitle(options.label, ds.original.auxCostAndTypes, ds.original.net_assets);
        ds.label = ds.original.auxCostAndTypes.map(c => {
          const bps = Utils.calculateBPS(c.amount, ds.original.net_assets);
          return `${c.type}: ${Utils.formatNumber(bps, '0.0a')} BPS`;
        });

        return ds;
      });
    }

    if (options.selectedUnit === 'percent') {
      const totalBPS = d.datasets.reduce((t, ds) => {
        const costTypeTotals = ds.original.auxCostAndTypes.map(c => c.amount);
        const costTotal = costTypeTotals.reduce((s, c) => s + c, 0);
        return t + Utils.calculateBPS(costTotal, ds.data[0].x);
      }, 0);

      d.datasets = d.datasets.map(ds => {
        const costTypeTotals = ds.original.auxCostAndTypes.map(c => c.amount);
        const costTotal = costTypeTotals.reduce((t, c) => t + c, 0);
        const bps = Utils.calculateBPS(ds.data[0].y, ds.data[0].x);

        ds.data[0] = {
          x: ds.data[0].x,
          y: Math.fround((bps / totalBPS) * 100),
        };

        ds.title = [options.label + ': ' + Utils.formatNumber(ds.data[0].y, '0.0a') + '% (Based on BPS)'];

        ds.label = ds.original.auxCostAndTypes.map(c => {
          const percent = Math.fround((c.amount / costTotal) * 100);
          return `${c.type}: ${Utils.formatNumber(percent, '0.0a')}%`;
        });

        return ds;
      });
    }

    return d;
  }

  forAdminComparision(d: any, options: { selectedUnit: string, label: string }): any {
    if (options.selectedUnit === 'gbp') {
      d.datasets = d.datasets.map(ds => {
        const costTypeTotals = ds.original.adminCostAndTypes.map(c => c.amount);
        const costTotal = costTypeTotals.reduce((t, c) => t + c, 0);
        ds.data[0] = {
          x: ds.data[0].x,
          y: costTotal,
        };

        ds.title = UnitTypeFilter.generateTitle(options.label, ds.original.adminCostAndTypes, ds.original.net_assets, 'gbp');
        ds.label = ds.original.adminCostAndTypes.map(c => {
          return `${c.type}: ${Utils.humanize(c.amount)}`;
        });

        return ds;
      });
    }

    if (options.selectedUnit === 'bps') {
      d.datasets = d.datasets.map(ds => {
        const costTypeTotals = ds.original.adminCostAndTypes.map(c => c.amount);
        const costTotal = costTypeTotals.reduce((t, c) => t + c, 0);

        ds.data[0] = {
          x: ds.data[0].x,
          y: Utils.calculateBPS(costTotal, ds.data[0].x),
        };

        ds.title = UnitTypeFilter.generateTitle(options.label, ds.original.adminCostAndTypes, ds.original.net_assets);
        ds.label = ds.original.adminCostAndTypes.map(c => {
          const bps = Utils.calculateBPS(c.amount, ds.original.net_assets);
          return `${c.type}: ${Utils.formatNumber(bps, '0.0a')} BPS`;
        });

        return ds;
      });
    }

    if (options.selectedUnit === 'percent') {
      const totalBPS = d.datasets.reduce((t, ds) => {
        const costTypeTotals = ds.original.adminCostAndTypes.map(c => c.amount);
        const costTotal = costTypeTotals.reduce((s, c) => s + c, 0);
        return t + Utils.calculateBPS(costTotal, ds.data[0].x);
      }, 0);

      d.datasets = d.datasets.map(ds => {
        const costTypeTotals = ds.original.adminCostAndTypes.map(c => c.amount);
        const costTotal = costTypeTotals.reduce((t, c) => t + c, 0);
        const bps = Utils.calculateBPS(ds.data[0].y, ds.data[0].x);

        ds.data[0] = {
          x: ds.data[0].x,
          y: Math.fround((bps / totalBPS) * 100),
        };

        ds.title = [options.label + ': ' + Utils.formatNumber(ds.data[0].y, '0.0a') + '% (Based on BPS)'];

        ds.label = ds.original.adminCostAndTypes.map(c => {
          const percent = Math.fround((c.amount / costTotal) * 100);
          return `${c.type}: ${Utils.formatNumber(percent, '0.0a')}%`;
        });

        return ds;
      });
    }

    return d;
  }

  forManagementTable(d: Array<any>, options: { selectedUnit: string, net_assets: number }): any {
    if (options.selectedUnit === 'bps') {
      d = d.map(r => {
        for (const k of Object.keys(r)) {
          if (k.startsWith('cat_')) {
            r[k] = Utils.calculateBPS(r[k], options.net_assets);
            if (k.endsWith('_filtered')) {
              r[k] = Utils.formatNumber(r[k.substr(0, k.lastIndexOf('_'))], '0.0a') + ' BPS';
            }
          }
        }
        r.total = Utils.calculateBPS(r.total, options.net_assets);
        r.formattedTotal = Utils.formatNumber(r.total, '0.0a') + ' BPS';
        return r;
      });
    }

    if (options.selectedUnit === 'percent') {
      const total = d.reduce((t, c) => t + c.total, 0);
      d = d.map(r => {
        for (const k of Object.keys(r)) {
          if (k.startsWith('cat_')) {
            r[k] = Math.fround((r[k] / total) * 100);
            if (k.endsWith('_filtered')) {
              r[k] = Utils.formatNumber(r[k.substr(0, k.lastIndexOf('_'))], '0.0a') + ' %';
            }
          }
        }

        r.total = Math.fround((r.total / total) * 100);
        r.formattedTotal = Utils.formatNumber(r.total, '0.0a') + '%';
        return r;
      });
    }

    return d;
  }

  forPerformanceComparision(d: any, options: { selectedUnit: string, label: string }): any {
    if (options.selectedUnit === 'gbp') {
      d.datasets = d.datasets.map(ds => {
        const costTypeTotals = ds.original.breakdown.map(c => c.amount);
        const costTotal = costTypeTotals.reduce((t, c) => t + c, 0);
        ds.data[0] = {
          x: costTotal,
          y: ds.data[0].y,
        };

        ds.label = ds.original.breakdown.map(c => {
          return `${c.name}: ${Utils.humanize(c.amount)}`;
        });
        const tot = costTotal;
        ds.footer[0] = `${options.label}: ${Utils.humanize(tot)}`;

        return ds;
      });
    }

    if (options.selectedUnit === 'bps') {
      d.datasets = d.datasets.map(ds => {
        const bps = Utils.calculateBPS(ds.data[0].x, ds.original.net_assets);

        ds.data[0] = {
          x: bps,
          y: ds.data[0].y,
        };

        ds.label = ds.original.breakdown.map(c => {
          const val = Utils.calculateBPS(c.amount, ds.original.net_assets);
          return `${c.name}: ${Utils.formatNumber(val, '0.0a')} BPS`;
        });
        ds.footer[0] = `${options.label}: ${Utils.formatNumber(bps, '0.0a')} BPS`;

        return ds;
      });
    }

    if (options.selectedUnit === 'percent') {
      const datasetTotal = d.datasets.reduce((t, ds) => t + ds.data[0].x, 0);

      d.datasets = d.datasets.map(ds => {
        const percentage = (ds.data[0].x / datasetTotal) * 100;

        ds.data[0] = {
          x: Math.fround(percentage),
          y: ds.data[0].y,
        };

        const total = ds.original.breakdown.reduce((t, c) => t + c.amount, 0);
        ds.label = ds.original.breakdown.map(c => {
          const percent = Math.fround((c.amount / total) * 100);
          return `${c.name}: ${Utils.formatNumber(percent, '0.0a')}%`;
        });
        ds.footer[0] = [options.label + ': ' + Utils.formatNumber(ds.data[0].x, '0') + '%'];

        return ds;
      });
    }

    return d;
  }

}
