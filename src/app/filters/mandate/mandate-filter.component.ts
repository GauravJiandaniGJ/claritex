import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-mandate-filter',
  templateUrl: './mandate-filter.component.html',
})
export class MandateFilterComponent implements OnInit {

  @Input()
  labelKey = 'name';

  @Input()
  dataKey = 'id';

  @Input()
  placeholder = 'Mandate';

  @Input()
  mandates: Array<any>;

  @Input()
  selected: Array<any> = [];

  @Output()
  change = new EventEmitter();

  constructor() {
    //
  }

  ngOnInit(): void {
  }

  onSelect(event) {
    this.change.emit(event);
  }

}
