import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-asset-class-filter',
  templateUrl: './asset-class-filter.component.html',
})
export class AssetClassFilterComponent implements OnInit {

  @Input()
  labelKey = 'name';

  @Input()
  dataKey = 'id';

  @Input()
  placeholder = 'Asset Class';

  @Input()
  assetClasses: Array<any>;

  @Input()
  selected: Array<any> = [];

  @Output()
  change = new EventEmitter();

  constructor() {
    //
  }

  ngOnInit(): void {
  }

  onSelect(event) {
    this.change.emit(event);
  }

}
