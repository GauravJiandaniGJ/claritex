import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-asset-manager-filter',
  templateUrl: './asset-manager-filter.component.html',
})
export class AssetManagerFilterComponent implements OnInit {

  @Input()
  labelKey = 'name';

  @Input()
  dataKey = 'id';

  @Input()
  placeholder = 'Asset Manager';

  @Input()
  managers: Array<any>;

  @Input()
  selected: Array<any> = [];

  @Output()
  change = new EventEmitter();

  constructor() {
    //
  }

  ngOnInit(): void {
  }

  onSelect(event) {
    this.change.emit(event);
  }

}
