import {Directive, ElementRef, OnInit} from '@angular/core';

@Directive({
  selector: '[appSticky]',
})
export class StickyDirective implements OnInit {

  private element;

  private stuck;

  private stickPoint: number;

  constructor(elRef: ElementRef) {
    this.element = elRef.nativeElement;
  }

  ngOnInit(): void {
    this.stickPoint = this.getDistance();
    window.addEventListener('scroll', this.calculate.bind(this));
  }

  calculate(e) {
    const offset = window.pageYOffset;
    const distance = this.getDistance() - offset;

    if ((distance <= 0) && !this.stuck) {
      this.element.classList.add('sticky');
      this.stuck = true;
    } else if (this.stuck && (offset <= this.stickPoint)) {
      this.element.classList.remove('sticky');
      this.stuck = false;
    }
  }

  getDistance() {
    return this.element.offsetTop + 10;
  }

}
