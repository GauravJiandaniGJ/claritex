import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SchemeService} from './services/scheme.service';
import {ChartTypesService} from './services/chart-types.service';
import {MandateService} from './services/mandate.service';
import {AuthGuard} from './services/auth.guard';
import {HttpClientModule} from '@angular/common/http';
import {UserService} from './services/user.service';
import {AuthService} from './services/auth.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  exports: [],
  declarations: [],
  providers: [
    UserService,
    AuthService,
    SchemeService,
    MandateService,
    ChartTypesService,
    AuthGuard,
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(`CoreModule has already been loaded. Import Core Modules in the AppModule only.`);
    }
  }

  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders> {
      ngModule: CoreModule,
      providers: [],
    };
  }
}
