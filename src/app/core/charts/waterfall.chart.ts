export class WaterfallChart {

  static register(Chart: any, config: any) {
    Chart.defaults.waterfall = Chart.defaults.bar;
    Chart.controllers.waterfall = Chart.controllers.bar.extend({
      calculateBarValuePixels: function (datasetIndex, index) {
        const me = this;
        const previous = index - 1;
        const chart = me.chart;
        const meta = me.getMeta();
        const scale = me.getValueScale();
        let datasets = chart.data.datasets;

        if (chart.data.constructor === Array) {
          datasets = [{
            data: chart.data,
          }];
        }

        let value = Number(datasets[datasetIndex].data[index]);

        if (previous > -1) {
          value = Number(datasets[datasetIndex].data[index]) - Number(datasets[datasetIndex].data[previous]);
        }

        const stacked = scale.options.stacked;
        const stack = meta.stack;
        // Use the min value of the range object
        let start = 0;

        if (previous > -1) {
          start = Number(datasets[datasetIndex].data[previous]);
        }
        let i, imeta, ivalue, base, head, size;

        if (stacked || (stacked === undefined && stack !== undefined)) {
          for (i = 0; i < datasetIndex; ++i) {
            imeta = chart.getDatasetMeta(i);
            if (imeta.bar && imeta.stack === stack
              && imeta.controller.getValueScaleId() === scale.id && chart.isDatasetVisible(i)) {
              ivalue = Number(datasets[i].data[index]);
              if ((value < 0 && ivalue < 0) || (value >= 0 && ivalue > 0)) {
                start += ivalue;
              }
            }
          }
        }

        let dataStart = start;
        let dataEnd = value;

        if (dataEnd < dataStart) {
          const temp = dataEnd;
          dataEnd = dataStart;
          dataStart = temp;
        }

        base = scale.getPixelForValue(dataStart);
        head = scale.getPixelForValue(dataEnd);
        size = (head - base) / 2;

        return {
          size: size,
          base: base,
          head: head,
          center: head + size / 2
        };
      },
      draw: function () {
        const me = this;
        const chart = me.chart;
        const elements = me.getMeta().data;
        const dataset = me.getDataset();
        const ilen = elements.length;
        let i = 0;
        let d;

        Chart.helpers.canvas.clipArea(chart.ctx, chart.chartArea);

        for (; i < ilen; ++i) {
          d = dataset.data[i];
          if (d !== null && d !== undefined && !isNaN(d)) {
            elements[i].draw();
          }
        }

        Chart.helpers.canvas.unclipArea(chart.ctx);
      }
    });
  }

}
