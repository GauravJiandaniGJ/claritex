import {Injectable} from '@angular/core';
import {Collection} from '../models/Collection';
import {HttpClient} from '@angular/common/http';
import {MandateCostTypeCategory} from '../models/MandateCostTypeCategory';
import {AssetClass} from '../models/AssetClass';
import {MandateCostType} from '../models/MandateCostType';
import {PensionSchemeMandate} from '../models/PensionSchemeMandate';
import {Mandate} from '../models/Mandate';
import {ManagementCostType} from '../models/management/ManagementCostType';

@Injectable()
export class MandateService {

  constructor(private http: HttpClient) {
    //
  }

  all(): Promise<Collection<Mandate>> {
    return this.http.get<Collection<Mandate>>('mandates').toPromise()
      .then(res => (new Collection<Mandate>(res, 'mandates')));
  }

  allByScheme(id: any): Promise<PensionSchemeMandate[]> {
    return this.http.get<PensionSchemeMandate[]>(`pension-scheme/${id}/mandates`).toPromise();
  }

  allCostCategories(): Promise<Collection<MandateCostTypeCategory>> {
    return this.http.get<Collection<MandateCostTypeCategory>>('mandate-cost-type-categories').toPromise()
      .then(res => (new Collection<MandateCostTypeCategory>(res, 'mandateCostTypeCategories')));
  }

  allCostTypes(): Promise<Collection<MandateCostType>> {
    return this.http.get<Collection<MandateCostType>>('mandate-cost-types').toPromise()
      .then(res => (new Collection<MandateCostType>(res, 'mandateCostTypes')));
  }

  allAssetClasses(): Promise<Collection<AssetClass>> {
    return this.http.get<Collection<AssetClass>>('asset-classes').toPromise()
      .then(res => (new Collection<AssetClass>(res, 'assetClasses')));
  }

  managementCostTypes(): Promise<Collection<ManagementCostType>> {
    return this.http.get<Collection<ManagementCostType>>('cost-types').toPromise()
      .then(res => (new Collection<ManagementCostType>(res, 'mandateCostTypes')));
  }

  managementRootCostTypes(): Promise<ManagementCostType[]> {
    return this.http.get<ManagementCostType[]>('root-cost-types').toPromise();
  }

}
