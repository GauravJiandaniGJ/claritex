import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AccessToken} from '../models/auth/AccessToken';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient) {
    //
  }

  public authenticate(email: String, password: String): Promise<AccessToken> {
    return this.http.post<AccessToken>('auth', {username: email, password: password}).toPromise();
  }

  logout() {
    return new Promise(resolve => {
      window.localStorage.removeItem('token');
      resolve();
    });
  }

}
