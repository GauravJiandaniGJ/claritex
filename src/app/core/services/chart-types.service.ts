import {ChartType} from '../../components/chart/ChartType';
import {Injectable} from '@angular/core';
import * as numeral from 'numeral';

@Injectable()
export class ChartTypesService {

  private static AVAILABLE_CHART_TYPES = [
    new ChartType(
      'pie',
      'Pie',
      'pie',
      'fas fa-chart-pie',
    ),
    new ChartType(
      'bar',
      'Bar',
      'bar',
      'fas fa-chart-bar',
      {
        legend: {
          display: false,
        },
        scales: {
          yAxes: [{
            scaleLabel: {
              display: true,
            },
            ticks: {
              autoSkip: false,
              beginAtZero: true,
              callback: (label) => numeral(label).format('0a'),
            }
          }],
          xAxes: [{
            gridLines: {
              display: false,
              drawOnChartArea: false,
            },
            ticks: {
              autoSkip: false,
              beginAtZero: true,
            }
          }],
        },
      }
    ),
    new ChartType(
      'scatter',
      'Scatter',
      'bubble',
      'fas fa-th',
      {
        elements: {
          point: {
            radius: ctx => 6 + (ctx.chart.width * 0.01),
          },
        }
      }
    ),
    new ChartType(
      'bubble',
      'Bubble',
      'bubble',
      'fas fa-braille',
    ),
    new ChartType(
      'waterfall',
      'Waterfall',
      'waterfall',
      'fas fa-chart-bar'
    ),
  ];

  public static availableChartTypes(): ChartType[] {
    return ChartTypesService.AVAILABLE_CHART_TYPES;
  }

  public breakdownChartTypes(): ChartType[] {
    const available = ['pie', 'bar'];
    return ChartTypesService.availableChartTypes()
      .filter(chartType => available.includes(chartType.id));
  }

  public waterfallChartTypes(): ChartType[] {
    const available = ['waterfall'];
    return ChartTypesService.availableChartTypes()
      .filter(chartType => available.includes(chartType.id));
  }

  public universeComparisionChartTypes(): ChartType[] {
    const available = ['scatter'];
    return ChartTypesService.availableChartTypes()
      .filter(chartType => available.includes(chartType.id));
  }

  public performanceBreakdownChartTypes(): ChartType[] {
    const available = ['pie', 'bar', 'waterfall'];
    return ChartTypesService.availableChartTypes()
      .filter(chartType => available.includes(chartType.id));
  }

}
