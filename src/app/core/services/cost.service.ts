import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Cost} from '../models/Cost';

@Injectable()
export class CostService {

  constructor(private http: HttpClient) {
  }

  getCost(): Promise<Cost>  {
    return this.http.get<Cost>('http://www.mocky.io/v2/5ad99c7f2f00005400cfde02')
      .toPromise();
  }

  getAllCost(): Promise<Cost>  {
    return this.http.get<Cost>(`5ad5c20f3200006a00202cd5`)
      .toPromise();
  }
}
