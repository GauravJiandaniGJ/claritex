import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PensionScheme} from '../models/PensionScheme';
import {Collection} from '../models/Collection';
import {PensionSchemeTotalCosts} from '../models/PensionSchemeTotalCosts';
import {UniverseTotalCostAndNetAsset} from '../models/UniverseTotalCostAndNetAsset';
import {AdminCostAndType} from '../models/AdminCostAndType';
import {AdminCostTypesAndNetAsset} from '../models/AdminCostTypesAndNetAsset';
import {AuxCostAndType} from '../models/AuxCostAndType';
import {AuxCostTypesAndNetAsset} from '../models/AuxCostTypesAndNetAsset';
import {InvestmentManagementCost} from '../models/investment/InvestmentManagementCost';
import {PensionSchemeManagementCost} from '../models/investment/PensionSchemeManagementCost';
import {ManagementCost} from '../models/management/ManagementCost';
import {Cost} from '../models/Cost';

@Injectable()
export class SchemeService {

  constructor(private http: HttpClient) {
  }

  all(): Promise<Collection<PensionScheme>> {
    return this.http.get<Collection<PensionScheme>>('pension-schemes')
      .toPromise()
      .then(res => (new Collection<PensionScheme>(res, 'pensionSchemes')));
  }

  get(id: any): Promise<PensionScheme> {
    return this.http.get<PensionScheme>(`pension-schemes/${id}`)
      .toPromise();
  }

  getTotalCosts(id: any): Promise<PensionSchemeTotalCosts> {
    return this.http.get<PensionSchemeTotalCosts>(`pension-scheme/${id}/total-costs`)
      .toPromise();
  }

  getUniverseTotalCostAndNetAssets(id: any): Promise<UniverseTotalCostAndNetAsset[]> {
    return this.http.get<UniverseTotalCostAndNetAsset[]>(`pension-scheme/${id}/universe-total-costs-net-assets`)
      .toPromise();
  }

  getAdminCostAndType(id: any): Promise<AdminCostAndType[]> {
    return this.http.get<AdminCostAndType[]>(`pension-scheme/${id}/admin-costs`)
      .toPromise();
  }

  getUniverseAdminCostTypeAndNetAssets(id: any): Promise<AdminCostTypesAndNetAsset[]> {
    return this.http.get<AdminCostTypesAndNetAsset[]>(`pension-scheme/${id}/universe-admin-costs-net-assets`)
      .toPromise();
  }

  getAuxCostAndType(id: any): Promise<AuxCostAndType[]> {
    return this.http.get<AuxCostAndType[]>(`pension-scheme/${id}/auxiliary-costs`)
      .toPromise();
  }

  getUniverseAuxCostTypeAndNetAssets(id: any): Promise<AuxCostTypesAndNetAsset[]> {
    return this.http.get<AuxCostTypesAndNetAsset[]>(`pension-scheme/${id}/universe-auxiliary-costs-net-assets`)
      .toPromise();
  }

  getUniverseInvestmentCosts(id: any): Promise<UniverseTotalCostAndNetAsset[]> {
    return this.http.get<UniverseTotalCostAndNetAsset[]>(`pension-scheme/${id}/universe-investment-costs`)
      .toPromise();
  }

  getInvestmentManagementCosts(id: any): Promise<InvestmentManagementCost> {
    return this.http.get<InvestmentManagementCost>(`pension-scheme/${id}/investment-management-costs`)
      .toPromise();
  }

  getManagementCosts(id: any, cost_type: number = 0): Promise<Cost> {
    return this.http.get<Cost>(`pension-scheme/${id}/management-costs/${cost_type}`)
      .toPromise();
  }

  getUniverseManagementCosts(id: any, type_id: number = 0): Promise<Cost> {
    return this.http.get<Cost>(`pension-scheme/${id}/universe-management-costs/${type_id}`)
      .toPromise();
  }

  getInvestmentManagementCostAndReturn(id: any): Promise<Cost> {
    return this.http.get<Cost>(`pension-scheme/${id}/investment-management-cost-returns`)
      .toPromise();
  }

  getAssetAllocation(id: any): Promise<Cost> {
    return this.http.get<Cost>(`pension-scheme/${id}/asset-allocation`)
      .toPromise();
  }

  getPerformance(id: any): Promise<Cost> {
    return this.http.get<Cost>(`pension-scheme/${id}/performance`)
      .toPromise();
  }
}
