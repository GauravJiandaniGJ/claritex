import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/User';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
    //
  }

  public getCurrentUser(): Promise<User> {
    return this.http.get<User>('user').toPromise();
  }

}
