import {Injectable} from '@angular/core';
import {CanActivate, CanActivateChild, Router} from '@angular/router';
import {UserService} from './user.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(private router: Router, private userService: UserService) {
    //
  }

  canActivate() {
    const token = window.localStorage.getItem('token');
    if (token) {
      return true;
    }

    this.router.navigate(['/auth']);

    return false;
  }

  canActivateChild() {
    return this.canActivate();
  }

}
