import {MandateCostTypeCategory} from '../MandateCostTypeCategory';
import {InvestmentCostType} from './InvestmentCostType';

import {Utils} from '../../../utils/Utils';

export class InvestmentCostCategory extends MandateCostTypeCategory {

  total: number;
  typeCosts: InvestmentCostType[];

  public static calculateTotal(costs: InvestmentCostCategory) {
    if (costs.id !== 2) {
      return costs;
    }

    costs.total = costs.typeCosts
      .map(m => InvestmentCostType.calculateTotal(m).total)
      .reduce((s, v) => s + v, 0);
    return costs;
  }
}
