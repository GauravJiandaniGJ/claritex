import {InvestmentManagerCost} from './InvestmentManager';

import {Utils} from '../../../utils/Utils';

export class InvestmentManagementCost {

  total: number;
  net_asset_amount: number;
  managers: InvestmentManagerCost[];

  public static calculateTotal(costs: InvestmentManagementCost): InvestmentManagementCost {
    costs.total = costs.managers
      .map(m => InvestmentManagerCost.calculateTotal(m).total)
      .reduce((s, v) => s + v, 0);
    return costs;
  }

}
