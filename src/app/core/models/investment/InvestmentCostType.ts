import {MandateCostType} from '../MandateCostType';

import {InvestmentCostTypeAsset} from './InvestmentCostTypeAsset';
import {Utils} from '../../../utils/Utils';

export class InvestmentCostType extends MandateCostType {

  total: number;
  assetCosts: InvestmentCostTypeAsset[];

  public static calculateTotal(costs: InvestmentCostType): InvestmentCostType {
    costs.total = costs.assetCosts.map(m => m.cost).reduce((s, v) => s + v, 0);
    return costs;
  }
}
