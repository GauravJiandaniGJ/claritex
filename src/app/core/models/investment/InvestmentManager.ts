import {AssetManager} from '../AssetManager';
import {InvestmentMandateCost} from './InvestmentMandateCost';

import {Utils} from '../../../utils/Utils';

export class InvestmentManagerCost extends AssetManager {

  total: number;
  mandateCosts: InvestmentMandateCost[];

  public static calculateTotal(costs: InvestmentManagerCost) {
    costs.total = costs.mandateCosts
      .map(m => InvestmentMandateCost.calculateTotal(m).total)
      .reduce((s, v) => s + v, 0);
    return costs;
  }
}
