import {Mandate} from '../Mandate';
import {InvestmentCostCategory} from './InvestmentCostCategory';

import {Utils} from '../../../utils/Utils';

export class InvestmentMandateCost extends Mandate {

  total: number;
  net_return: number;
  gross_return: number;
  categoryCosts: InvestmentCostCategory[];

  public static calculateTotal(costs: InvestmentMandateCost) {
    costs.total = costs.categoryCosts
      .map(m => InvestmentCostCategory.calculateTotal(m).total)
      .reduce((s, v) => s + v, 0);
    return costs;
  }
}
