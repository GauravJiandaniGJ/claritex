import {Model} from './Model';

import {AssetClass} from './AssetClass';

export class InvestmentAssetCost extends Model {
  amount: number;
  assetClass: AssetClass;
}
