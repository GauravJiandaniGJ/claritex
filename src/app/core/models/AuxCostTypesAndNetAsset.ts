import {Model} from './Model';
import {AuxCostAndType} from './AuxCostAndType';


export class AuxCostTypesAndNetAsset extends Model {
  net_assets: number;
  auxCostAndTypes: AuxCostAndType[];
}
