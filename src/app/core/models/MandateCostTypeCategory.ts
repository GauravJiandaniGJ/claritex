import {Model} from './Model';

export class MandateCostTypeCategory extends Model {
  id: number;
  name: string;
}
