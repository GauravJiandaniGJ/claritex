export enum CostEntity {
    SCHEME = 'scheme',
    MANAGER = 'manager',
    MANDATE = 'mandate',
    COST = 'cost',
    ASSET = 'asset'
}
