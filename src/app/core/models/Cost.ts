import {CostEntity} from './CostEntity';

export class Cost {
  _entity: CostEntity;
  id: number;
  name: string;
  amount: number;
  breakdown: Cost[] = [];
  info: any;
}


