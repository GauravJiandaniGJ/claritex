import {Model} from './Model';

import {MandateCostType} from './MandateCostType';
import {InvestmentAssetCost} from './InvestmentAssetCost';

export class InvestmentTypeCost extends Model {
  total: number;
  type: MandateCostType;
  assetsCosts: InvestmentAssetCost[];
}
