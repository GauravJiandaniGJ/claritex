import {Model} from './Model';
import {Mandate} from './Mandate';

export class AssetManager extends Model {
  id: number;
  name: string;
  mandates: Mandate[];
}
