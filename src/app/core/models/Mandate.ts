import {Model} from './Model';
import {MandateInvestmentCost} from './MandateInvestmentCost';

export class Mandate extends Model {
  id: number;
  name: string;
  compliance: boolean;
  investmentCosts: MandateInvestmentCost[];
}
