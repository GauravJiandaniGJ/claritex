import {Model} from './Model';
import {AdminCostAndType} from './AdminCostAndType';


export class AdminCostTypesAndNetAsset extends Model {
  net_assets: number;
  adminCostAndTypes: AdminCostAndType[];
}
