import {Model} from './Model';
import {Mandate} from './Mandate';

import {InvestmentCategoryCost} from './InvestmentCategoryCost';

export class MandateInvestmentCost extends Model {
  mandate: Mandate;
  total: number;
  categoryCosts: InvestmentCategoryCost[];
}
