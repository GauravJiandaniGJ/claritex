import {Mandate} from './Mandate';

export class PensionSchemeMandate extends Mandate {

  pension_scheme_id: number;

  template: string;

  created_at: Date;

}
