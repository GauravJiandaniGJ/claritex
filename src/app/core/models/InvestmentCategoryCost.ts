import {Model} from './Model';

import {InvestmentTypeCost} from './InvestmentTypeCost';
import {MandateCostTypeCategory} from './MandateCostTypeCategory';

export class InvestmentCategoryCost extends Model {
  total: number;
  category: MandateCostTypeCategory;
  typeCosts: InvestmentTypeCost[];
}
