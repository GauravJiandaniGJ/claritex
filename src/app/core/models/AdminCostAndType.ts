import {Model} from './Model';

export class AdminCostAndType extends Model {
  id: number;
  type: string;
  type_id: number;
  amount: number;
  pension_scheme_id: number;
}
