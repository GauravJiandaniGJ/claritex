import {Model} from './Model';

export class User extends Model {

  public id: number;

  public name: string;

  public email: string;

  public enabled: boolean;

  public authorities = [];

}
