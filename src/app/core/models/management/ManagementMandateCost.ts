import {Mandate} from '../Mandate';

import {Utils} from '../../../utils/Utils';
import {ManagementCostType} from './ManagementCostType';

export class ManagementMandateCost extends Mandate {

  total: number;
  costTypes: ManagementCostType[];

  public static calculateTotal(costs: ManagementMandateCost) {
    costs.total = costs.costTypes
      .map(m => ManagementCostType.calculateTotal(m).total)
      .reduce((s, v) => s + v, 0);
    return costs;
  }
}
