import {ManagementManagerCost} from './ManagementManagerCost';

export class ManagementCost {

  total: number;
  net_asset_amount: number;
  managers: ManagementManagerCost[];

  public static calculateTotal(costs: ManagementCost): ManagementCost {
    costs.total = costs.managers
      .map(m => ManagementManagerCost.calculateTotal(m).total)
      .reduce((s, v) => s + v, 0);
    return costs;
  }

}
