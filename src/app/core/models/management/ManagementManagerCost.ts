import {AssetManager} from '../AssetManager';
import {ManagementMandateCost} from './ManagementMandateCost';

export class ManagementManagerCost extends AssetManager {

  total: number;
  mandateCosts: ManagementMandateCost[];

  public static calculateTotal(costs: ManagementManagerCost) {
    costs.total = costs.mandateCosts
      .map(m => ManagementMandateCost.calculateTotal(m).total)
      .reduce((s, v) => s + v, 0);
    return costs;
  }
}
