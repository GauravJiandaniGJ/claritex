import {Model} from '../Model';
import {ManagementCost} from './ManagementCost';

export class PensionSchemeManagementCost extends Model {

  total: number;
  pensionSchemes: ManagementCost[];

  public static calculateTotal(costs: PensionSchemeManagementCost): PensionSchemeManagementCost {
    costs.total = costs.pensionSchemes
      .map(m => ManagementCost.calculateTotal(m).total)
      .reduce((s, v) => s + v, 0);
    return costs;
  }

}
