import {MandateCostType} from '../MandateCostType';

import {ManagementCostTypeAsset} from './ManagementCostTypeAsset';

export class ManagementCostType extends MandateCostType {

  total: number;
  children: ManagementCostType[];
  assetCosts: ManagementCostTypeAsset[];

  public static calculateTotal(costs: ManagementCostType): ManagementCostType {

    if (!costs.children || !costs.children.length) {
      return costs;
    }

    costs.total = costs.children
      .map(m => ManagementCostType.calculateTotal(m))
      .reduce((s, v) => s + v.total, 0);

    return costs;
  }
}
