import {Cost} from './Cost';

export class CostTreeCalculator {
  public static calculate(cost: Cost): Cost {
    if (cost.breakdown && cost.breakdown.length > 0) {
      cost.amount = cost.breakdown.reduce((total, childCost) => {
        // Calculate the Cost Tree (Recursively)
        CostTreeCalculator.calculate(childCost);
        // Return the accumulated total
        return total + childCost.amount;
      }, 0);

      if (!cost.info) {
        cost.info = {};
      }

      cost.info.target_amount = cost.breakdown.reduce((total, childCost) => {
        // Calculate the Cost Tree (Recursively)
        CostTreeCalculator.calculate(childCost);

        const amt = childCost.info.target_amount ? childCost.info.target_amount : 0;
        // Return the accumulated total
        return total + amt;
      }, 0);
    }
    return cost;
  }
}
