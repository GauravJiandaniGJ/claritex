import {Model} from './Model';


export class PensionSchemeTotalCosts extends Model {
  total_cost: number;
  admin_cost: number;
  investment_cost: number;
  investment_auxiliary_cost: number;
  investment_management_cost: number;
  net_asset_amount: number;
}
