import {Model} from './Model';

export class PensionScheme extends Model {
  id: number;
  name: string;
  country: string;
}
