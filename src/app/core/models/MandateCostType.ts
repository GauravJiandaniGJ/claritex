import {Model} from './Model';

export class MandateCostType extends Model {
  id: number;
  name: string;
  category_id: number;
}
