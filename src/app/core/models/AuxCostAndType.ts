import {Model} from './Model';


export class AuxCostAndType extends Model {
  id: number;
  type: string;
  type_id: number;
  amount: number;
  pension_scheme_id: number;
}
