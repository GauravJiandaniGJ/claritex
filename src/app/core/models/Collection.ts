export class Collection<M> {

  private static KEY = '_embedded';

  private static LINKS_KEY: '_links';

  constructor(private response: any, private defaultResource?: string) {
    //
  }

  all() {
    return this.embedded(this.defaultResource);
  }

  embedded(key?: string): M[] | any {
    const collection = this.response[Collection.KEY];

    if (key) {
      return <M[]> collection[key];
    }

    return collection;
  }

  links(): object {
    return this.response[Collection.LINKS_KEY];
  }

  page(): object {
    return this.response['page'];
  }
}
