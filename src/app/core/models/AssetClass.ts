import {Model} from './Model';

export class AssetClass extends Model {
  id: number;
  name: string;
}
