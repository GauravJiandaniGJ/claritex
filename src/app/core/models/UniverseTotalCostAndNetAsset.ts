import {Model} from './Model';


export class UniverseTotalCostAndNetAsset extends Model {
  total_costs: number;
  net_assets: number;
}
