import {Component, OnInit} from '@angular/core';
import {PensionScheme} from '../../../core/models/PensionScheme';
import {ActivatedRoute} from '@angular/router';
import {SchemeService} from '../../../core/services/scheme.service';
import {PensionSchemeTotalCosts} from '../../../core/models/PensionSchemeTotalCosts';
import merge from 'deepmerge';
import {UnitTypeFilter} from '../../../filters/unit-type/unit-type-filter';
import {AssetManager} from '../../../core/models/AssetManager';
import {Mandate} from '../../../core/models/Mandate';
import {MandateCostTypeCategory} from '../../../core/models/MandateCostTypeCategory';
import {MandateService} from '../../../core/services/mandate.service';
import {Utils} from '../../../utils/Utils';
import {AssetClass} from '../../../core/models/AssetClass';
import {ManagementCostType} from '../../../core/models/management/ManagementCostType';
import {Cost} from '../../../core/models/Cost';
import {CostTreeCalculator} from '../../../core/models/CostTreeCalculator';

class PerformanceFilters {
  managerFilter: Function = (o) => o;
  mandateFilter: Function = (o) => o;
  categoryFilter: Function = (o) => o;
  typeFilter: Function = (o) => o;
  assetFilter: Function = (o) => o;
}

@Component({
  selector: 'app-integrated-report',
  templateUrl: './integrated-report.component.html',
})
export class IntegratedReportComponent implements OnInit {

  scheme: PensionScheme;

  costCategories: MandateCostTypeCategory[];

  assetClasses: AssetClass[];

  managers: AssetManager[];

  mandates: Mandate[];

  selectedUnit = 'percent';

  filteredRows = [];

  columns = [];

  selectedCostCategories = [];

  selectedManagers = [];

  selectedMandates = [];

  selectedAssetClasses = [];

  selectedCostTypes = [];

  costTypes: ManagementCostType[];

  private rows = [];

  private comparisionFilters: Array<Function> = [];

  private totalCosts: PensionSchemeTotalCosts;

  private managementCosts: Cost;

  private filteredManagementCosts: Cost;

  private defaultFilter: PerformanceFilters;

  private tableFilters: Array<Function> = [];

  private netComparisionData;

  private grossComparisionData;

  private clientComparisionData;

  filteredNetComparisionData;

  filteredGrossComparisionData;

  filteredClientComparisionData;

  constructor(private route: ActivatedRoute, private schemeService: SchemeService,
              private mandateService: MandateService, private unitTypeFilter: UnitTypeFilter) {

    this.columns = [
      {name: 'Mandate', prop: 'mandate'},
      {name: 'Investment Management Costs', prop: 'total'},
      {name: 'Gross Return', prop: 'gross_return'},
      {name: 'Net Return', prop: 'net_return'},
      {name: 'Client Return', prop: 'client_return'},
      {name: 'Total (Formatted)', prop: 'formattedTotal'},
    ];

    this.makeDefaultFilter();
    this.tableFilters.push(unitTypeFilter.forManagementTable);
    this.comparisionFilters.push(unitTypeFilter.forPerformanceComparision);
  }

  ngOnInit() {
    this.route.parent.data.subscribe((data: { pensionScheme: PensionScheme }) => {
      this.scheme = data.pensionScheme;

      this.schemeService.getTotalCosts(this.scheme.id).then((tc: PensionSchemeTotalCosts) => {
        this.totalCosts = tc;
      })
      // .then(this.fetchCostCategories.bind(this))
        .then(this.fetchCostTypes.bind(this))
        .then(this.fetchAssetClasses.bind(this))
        .then(this.fetchManagementCosts.bind(this));
    });
  }

  public get pageLoaded() {
    return this.scheme && this.filteredNetComparisionData && this.filteredRows;
  }

  public fetchCostCategories() {
    return this.mandateService.allCostCategories()
      .then(collection => this.costCategories = collection.all());
  }

  public fetchCostTypes() {
    return this.mandateService.managementRootCostTypes()
      .then(collection => this.costTypes = collection);
  }

  public fetchAssetClasses() {
    return this.mandateService.allAssetClasses()
      .then(collection => this.assetClasses = collection.all());
  }

  public fetchManagementCosts() {
    return this.schemeService.getInvestmentManagementCostAndReturn(this.scheme.id)
      .then((costs: Cost) => {
        this.managementCosts = costs;
      })
      .then(() => this.managers = merge({d: [...this.managementCosts.breakdown]}, {}).d)
      .then(() => {
        this.mandates = [].concat.apply([], this.managementCosts.breakdown.map(m => m.breakdown));
      })
      .then(this.prepareData.bind(this))
      .then(this.renderBreakdownTable.bind(this))
      .then(this.renderComparisionCharts.bind(this));
  }

  public prepareData() {
    const allManagementCosts = merge({...this.managementCosts}, {});
    allManagementCosts.breakdown = this.filterData(allManagementCosts.breakdown, this.defaultFilter);
    this.filteredManagementCosts = CostTreeCalculator.calculate(allManagementCosts);
  }

  public renderBreakdownTable() {
    this.rows = [].concat.apply([], this.filteredManagementCosts.breakdown.map(m => m.breakdown))
      .map(mandate => {
        return {
          mandate: mandate.name,
          total: mandate.amount,
          formattedTotal: Utils.wholeNum(mandate.amount),
          net_return: mandate.info.net_return,
          formatted_net_return: `${mandate.info.net_return}%`,
          gross_return: mandate.info.gross_return,
          formatted_gross_return: `${mandate.info.gross_return}%`,
          client_return: mandate.info.client_return,
          formatted_client_return: `${mandate.info.client_return}%`,
        };
      });
    this.filterBreakdownTable();
  }

  public generateLabel(costTypes: Cost[]) {
    return costTypes.map(c => `${c.name}: ${Utils.humanize(c.amount)}`);
  }

  public renderComparisionCharts() {
    const mandates = [].concat.apply([], this.filteredManagementCosts.breakdown.map(m => m.breakdown));
    const colors = Utils.randomColor({count: mandates.length});

    this.renderNetComparisionChart(mandates, colors);
    this.renderGrossComparisionChart(mandates, colors);
    this.renderClientComparisionChart(mandates, colors);

    this.filterComparisionData();
  }

  public renderNetComparisionChart(mandates = [], colors = []) {
    const datasets = mandates.map((mandateCost, mandateIndex) => {
      const net_return = mandateCost.info.net_return;
      const net_asset_amount = mandateCost.info.net_asset_amount;
      const investmentCost = mandateCost.amount;

      return {
        pointRadius: 8,
        pointHoverRadius: 10,
        backgroundColor: colors[mandateIndex],
        title: `Mandate: ${mandateCost.name}`,
        label: this.generateLabel(mandateCost.breakdown),
        footer: [`Investment Management Costs: ${Utils.humanize(investmentCost)}`, `Net Return Percentage: ${net_return}%`],
        original: merge({...mandateCost}, {net_assets: net_asset_amount}),
        data: [{
          y: net_return,
          x: investmentCost,
        }]
      };
    });

    this.netComparisionData = {
      datasets: datasets,
    };
  }

  public renderGrossComparisionChart(mandates = [], colors = []) {
    const datasets = mandates.map((mandateCost, mandateIndex) => {
      const gross_return = mandateCost.info.gross_return;
      const investmentCost = mandateCost.amount;
      const net_asset_amount = mandateCost.info.net_asset_amount;

      return {
        pointRadius: 8,
        pointHoverRadius: 10,
        backgroundColor: colors[mandateIndex],
        title: `Mandate: ${mandateCost.name}`,
        label: this.generateLabel(mandateCost.breakdown),
        footer: [`Investment Management Costs: ${Utils.humanize(investmentCost)}`, `Gross Return Percentage: ${gross_return}%`],
        original: merge({...mandateCost}, {net_assets: net_asset_amount}),
        data: [{
          y: gross_return,
          x: investmentCost,
        }]
      };
    });

    this.grossComparisionData = {
      datasets: datasets,
    };
  }

  public renderClientComparisionChart(mandates = [], colors = []) {
    const datasets = mandates.map((mandateCost, mandateIndex) => {
      const client_return = mandateCost.info.client_return;
      const investmentCost = mandateCost.amount;
      const net_asset_amount = mandateCost.info.net_asset_amount;

      return {
        pointRadius: 8,
        pointHoverRadius: 10,
        backgroundColor: colors[mandateIndex],
        title: `Mandate: ${mandateCost.name}`,
        label: this.generateLabel(mandateCost.breakdown),
        footer: [`Investment Management Costs: ${Utils.humanize(investmentCost)}`, `Client Return Percentage: ${client_return}%`],
        original: merge({...mandateCost}, {net_assets: net_asset_amount}),
        data: [{
          y: client_return,
          x: investmentCost,
        }]
      };
    });

    this.clientComparisionData = {
      datasets: datasets,
    };
  }

  filterByAssetManager(event) {
    this.selectedManagers = event.value;

    if (this.selectedManagers.length) {
      this.mandates = [].concat.apply([], this.selectedManagers.map(m => m.breakdown));
    } else {
      this.mandates = [].concat.apply([], this.managementCosts.breakdown.map(m => m.breakdown));
    }

    this.prepareData();
    this.renderBreakdownTable();
    this.renderComparisionCharts();
  }

  filterByMandate(event) {
    this.selectedMandates = event.value;
    this.prepareData();
    this.renderBreakdownTable();
    this.renderComparisionCharts();
  }

  filterByCostCategory(event) {
    this.selectedCostCategories = event.value;
    this.prepareData();
    this.renderBreakdownTable();
    this.renderComparisionCharts();
  }

  filterByCostType(event) {
    this.selectedCostTypes = event.value;
    this.prepareData();
    this.renderBreakdownTable();
    this.renderComparisionCharts();
  }

  filterByAssetClass(event) {
    this.selectedAssetClasses = event.value;
    this.prepareData();
    this.renderBreakdownTable();
    this.renderComparisionCharts();
  }

  filterByUnitType(event) {
    this.selectedUnit = event.value;
    this.prepareData();
    this.renderBreakdownTable();
    this.renderComparisionCharts();
  }

  private filterBreakdownTable() {
    const data = merge({data: [...this.rows]}, {});
    const net_asset_amount = this.totalCosts.net_asset_amount ? this.totalCosts.net_asset_amount : 0;

    this.filteredRows = this.tableFilters.reduce((d, filter) => {
      return filter(d, {
        net_assets: net_asset_amount,
        selectedUnit: this.selectedUnit,
        selectedCostCategories: this.selectedCostCategories,
      });
    }, data.data);
  }

  private filterComparisionData() {
    this.filterNetComparisionData();
    this.filterGrossComparisionData();
    this.filterClientComparisionData();
  }

  private filterNetComparisionData() {
    const data = merge({...this.netComparisionData}, {});

    this.filteredNetComparisionData = this.comparisionFilters.reduce((d, filter) => filter(d, {
      label: 'Investment Management Costs',
      selectedUnit: this.selectedUnit,
      selectedCostTypes: this.selectedCostTypes,
    }), data);
  }

  private filterGrossComparisionData() {
    const data = merge({...this.grossComparisionData}, {});

    this.filteredGrossComparisionData = this.comparisionFilters.reduce((d, filter) => filter(d, {
      label: 'Investment Management Costs',
      selectedUnit: this.selectedUnit,
      selectedCostTypes: this.selectedCostTypes,
    }), data);
  }

  private filterClientComparisionData() {
    const data = merge({...this.clientComparisionData}, {});

    this.filteredClientComparisionData = this.comparisionFilters.reduce((d, filter) => filter(d, {
      label: 'Investment Management Costs',
      selectedUnit: this.selectedUnit,
      selectedCostTypes: this.selectedCostTypes,
    }), data);
  }

  private filterData(managers: Cost[], filters?: PerformanceFilters) {
    if (!filters) {
      filters = new PerformanceFilters();
    }

    filters = merge(new PerformanceFilters(), filters);

    return managers.filter(m => {
      m.breakdown = m.breakdown.filter(mc => {
        mc.breakdown = this.filterCostTypesRecursively(mc.breakdown, filters);
        // Filter Mandate Costs
        return this.filterMandates(mc, filters.mandateFilter);
      });
      // Filter Managers
      return this.filterManagers(m, filters.managerFilter);
    });
  }

  private filterManagers(m: Cost, filter?: Function): boolean {
    if (filter) {
      return filter(m);
    }
    return true;
  }

  private filterMandates(mc: Cost, filter?: Function): boolean {
    if (filter) {
      return filter(mc);
    }
    return true;
  }

  private filterCostTypes(tc: Cost, filter?: Function): boolean {
    if (filter) {
      return filter(tc);
    }
    return true;
  }

  private filterAssetClasses(ac: Cost, filter?: Function): boolean {
    if (filter) {
      return filter(ac);
    }
    return true;
  }

  private filterForCategory(cat: number): PerformanceFilters {
    const filter = new PerformanceFilters();
    filter.categoryFilter = (c => c.id === cat);
    return filter;
  }

  private calculateCategoryTotals(categories: Cost[]) {
    return categories.map(cat => {
      return this.calculateTotalForCategory(cat);
    });
  }

  private calculateTotalForCategory(cat: Cost) {
    const managementCost = merge({...this.filteredManagementCosts}, {});
    managementCost.breakdown = this.filterData(managementCost.breakdown, this.filterForCategory(cat.id));
    return (CostTreeCalculator.calculate(managementCost)).amount;
  }

  private makeDefaultFilter() {
    const filter = new PerformanceFilters();
    filter.categoryFilter = (m => {
      if (!this.selectedCostCategories.length) {
        return true;
      }
      return this.selectedCostCategories.some(mm => m.id === mm.id);
    });
    filter.managerFilter = (m => {
      if (!this.selectedManagers.length) {
        return true;
      }
      return this.selectedManagers.some(mm => m.id === mm.id);
    });
    filter.mandateFilter = (m => {
      if (!this.selectedMandates.length) {
        return true;
      }
      return this.selectedMandates.some(mm => m.id === mm.id);
    });
    filter.typeFilter = (m => {
      if (!this.selectedCostTypes.length) {
        return true;
      }
      return this.selectedCostTypes.some(mm => m.id === mm.id);
    });
    filter.assetFilter = (m => {
      if (!this.selectedAssetClasses.length) {
        return true;
      }
      return this.selectedAssetClasses.some(mm => m.id === mm.id);
    });

    this.defaultFilter = filter;
  }

  private filterCostTypesRecursively(costTypes: Cost[], filters: PerformanceFilters) {
    return costTypes.filter(ct => this.filterCostTypes(ct, filters.typeFilter));
  }

}
