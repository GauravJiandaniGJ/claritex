import {Component, OnInit, ViewChild} from '@angular/core';
import {PensionScheme} from '../../../core/models/PensionScheme';
import {ActivatedRoute} from '@angular/router';
import {AdminCostAndType} from '../../../core/models/AdminCostAndType';
import {SchemeService} from '../../../core/services/scheme.service';
import {PensionSchemeTotalCosts} from '../../../core/models/PensionSchemeTotalCosts';
import {Utils} from '../../../utils/Utils';
import merge from 'deepmerge';
import {UnitTypeFilter} from '../../../filters/unit-type/unit-type-filter';
import {CostTypeFilter} from '../../../filters/cost-type/cost-type-filter';
import {Cost} from '../../../core/models/Cost';
import {CostTreeCalculator} from '../../../core/models/CostTreeCalculator';

declare var window: any;

@Component({
  selector: 'app-asset-allocation',
  templateUrl: './asset-allocation.component.html',
})
export class AssetAllocationComponent implements OnInit {

  @ViewChild('allocationTable') private table: any;

  @ViewChild('portfolioDetailRow') private portfolioDetailRow: any;

  @ViewChild('portfolioTable') private portfolioTable: any;

  unitTypes = [
    {
      label: '%',
      value: 'percent',
    },
    {
      label: '£',
      value: 'gbp',
    }
  ];

  scheme: PensionScheme;

  adminCostAndTypes: AdminCostAndType[];

  filteredBreakdownData: any;

  filteredRows = [];

  selectedUnit = 'gbp';

  selectedCostTypes = [];

  private rows = [];

  public assetAllocations: Cost;

  breakdownWeightData: any;

  breakdownTargetData: any;

  private totalCosts: PensionSchemeTotalCosts;

  private breakdownFilters: Array<Function> = [];

  public filteredBreakdownTargetData: any;

  constructor(private route: ActivatedRoute, private schemeService: SchemeService,
              private unitTypeFilter: UnitTypeFilter, private costTypeFilter: CostTypeFilter) {
    this.unitTypes = [
      {
        label: '%',
        value: 'percent',
      },
      {
        label: '£',
        value: 'gbp',
      }
    ];
    // Unit Type Filters
    this.breakdownFilters.push(unitTypeFilter.forBreakdown);

  }

  ngOnInit() {
    this.route.parent.data.subscribe((data: { pensionScheme: PensionScheme }) => {
      this.scheme = data.pensionScheme;
      this.schemeService.getTotalCosts(this.scheme.id).then((tc: PensionSchemeTotalCosts) => {
        this.totalCosts = tc;
      })
        .then(this.fetchData.bind(this));
    });
  }

  print(divName) {
    //
  }

  public get pageLoaded() {
    return this.scheme && this.filteredBreakdownData && this.filteredRows;
  }

  private fetchData() {
    this.fetchAssetAllocation().then(assetAllocations => {
      this.assetAllocations = CostTreeCalculator.calculate(assetAllocations);
      this.renderTable();
      this.renderBreakdownChart();
    });
  }

  public renderTable() {
    this.rows = this.assetAllocations.breakdown.map(strategy => {
      return {
        id: strategy.id,
        name: strategy.name,
        total: strategy.amount,
        strategy: strategy,
        formattedTotal: Utils.wholeNum(strategy.amount),
      };
    });

    const data = merge({data: [...this.rows]}, {});

    this.filteredRows = data.data;
  }

  public renderBreakdownChart() {
    const breakdownData = this.assetAllocations.breakdown.reduce((m, alloc) => m.concat(alloc.breakdown), []);
    this.breakdownWeightData = {
      labels: breakdownData.map(alloc => alloc.name),
      original: merge({data: this.assetAllocations}, {}).data,
      datasets: [{
        data: [...breakdownData.map(strategy => strategy.amount)],
        formatted: [...breakdownData.map(strategy => Utils.humanize(strategy.amount))],
        backgroundColor: ['#114D64', 'orange', 'dodgerblue', '#176C89', '#8E8E80', '#C2A878', '#30343F'],
      }],
    };
    this.breakdownTargetData = {
      labels: breakdownData.map(alloc => alloc.name),
      original: merge({data: this.assetAllocations}, {}).data,
      datasets: [{
        data: [...breakdownData.map(strategy => strategy.info.target_amount)],
        formatted: [...breakdownData.map(strategy => Utils.humanize(strategy.info.target_amount))],
        backgroundColor: ['#114D64', 'orange', 'dodgerblue', '#176C89', '#8E8E80', '#C2A878', '#30343F'],
      }],
    };
    this.filterBreakdownData();
  }

  private filterBreakdownData() {
    const data = merge({...this.breakdownWeightData}, {});
    const net_asset_amount = this.totalCosts.net_asset_amount ? this.totalCosts.net_asset_amount : 0;

    this.filteredBreakdownData = this.breakdownFilters.reduce((d, filter) => filter(d, {
      net_assets: net_asset_amount,
      selectedUnit: 'percent',
      selectedCostTypes: this.selectedCostTypes,
    }), data);

    const targetData = merge({...this.breakdownTargetData}, {});
    this.filteredBreakdownTargetData = this.breakdownFilters.reduce((d, filter) => filter(d, {
      net_assets: net_asset_amount,
      selectedUnit: 'percent',
    }), targetData);
  }

  toggleDetail(event) {
    //
  }

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  togglePortfolioRow(row) {
    this.portfolioTable.rowDetail.toggleExpandRow(row);
  }

  private fetchAssetAllocation() {
    return this.schemeService.getAssetAllocation(this.scheme.id);
  }

  formatTotal(total, parentTotal) {
    if (this.selectedUnit === 'percent') {
      return this.formatTotalPercent(total, parentTotal);
    }

    return Utils.labelForUnit(total, this.selectedUnit, '0,0');
  }

  formatTotalPercent(total, parentTotal) {
    return Utils.labelForUnit((total / parentTotal) * 100, 'percent', '0,0.[0]');
  }

  formatRelative(current, parent = null) {
    if (!parent) {
      parent = this.assetAllocations;
    }
    const currentTotal = (current.amount / parent.amount) * 100;
    const parentTotal = (current.info.target_amount / parent.info.target_amount) * 100;

    return Utils.labelForUnit(currentTotal - parentTotal, 'percent', '0,0.[0]');
  }

  filterByUnitType(event) {
    this.selectedUnit = event.value;
    this.renderTable();
    this.renderBreakdownChart();
  }
}
