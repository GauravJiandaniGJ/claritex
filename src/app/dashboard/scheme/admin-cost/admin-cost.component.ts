import {Component, OnInit} from '@angular/core';
import {PensionScheme} from '../../../core/models/PensionScheme';
import {ActivatedRoute} from '@angular/router';
import {AdminCostAndType} from '../../../core/models/AdminCostAndType';
import {SchemeService} from '../../../core/services/scheme.service';
import {AdminCostTypesAndNetAsset} from '../../../core/models/AdminCostTypesAndNetAsset';
import {PensionSchemeTotalCosts} from '../../../core/models/PensionSchemeTotalCosts';
import {Utils} from '../../../utils/Utils';
import merge from 'deepmerge';
import {UnitTypeFilter} from '../../../filters/unit-type/unit-type-filter';
import {CostTypeFilter} from '../../../filters/cost-type/cost-type-filter';

import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../../store/state/IAppState';
import {ADD_ADMIN_COST_AND_TYPE} from '../../../store/actions';


@Component({
  selector: 'app-admin-cost',
  templateUrl: './admin-cost.component.html',
  styleUrls: ['./admin-cost.component.scss']
})
export class AdminCostComponent implements OnInit {

  scheme: PensionScheme;

  adminCostAndTypes: AdminCostAndType[];

  filteredBreakdownData: any;

  filteredComparisionData: any;

  filteredRows = [];

  columns = [];

  selectedUnit = 'gbp';

  selectedCostTypes = [];

  totalFilteredAmount: string;

  private rows = [];

  private breakdownData: any;

  private comparisionData: any;

  private adminCostTypesAndNetAssets: AdminCostTypesAndNetAsset[];

  private totalCosts: PensionSchemeTotalCosts;

  private breakdownFilters: Array<Function> = [];

  private comparisionFilters: Array<Function> = [];

  private tableFilters: Array<Function> = [];

  constructor(private route: ActivatedRoute, private schemeService: SchemeService,
              private unitTypeFilter: UnitTypeFilter, private costTypeFilter: CostTypeFilter,
              private ngRedux: NgRedux<IAppState>) {
    this.columns = [
      {name: 'Category', prop: 'category'},
      {name: 'Amount', prop: 'amount'},
      {name: 'Amount (Formatted)', prop: 'formattedAmount'},
    ];

    // Cost Type Filters
    this.breakdownFilters.push(costTypeFilter.forBreakdown);
    this.comparisionFilters.push(costTypeFilter.forAdminComparision);
    this.tableFilters.push(costTypeFilter.forTable);

    // Unit Type Filters
    this.breakdownFilters.push(unitTypeFilter.forBreakdown);
    this.comparisionFilters.push(unitTypeFilter.forAdminComparision);
    this.tableFilters.push(unitTypeFilter.forTable);
  }

  ngOnInit() {
    this.route.parent.data.subscribe((data: { pensionScheme: PensionScheme }) => {
      this.scheme = data.pensionScheme;
      this.schemeService.getTotalCosts(this.scheme.id).then((tc: PensionSchemeTotalCosts) => {
        this.totalCosts = tc;
      }).then(this.renderBreakdownChart.bind(this));
    });
  }

  public get pageLoaded() {
    return this.scheme && this.filteredBreakdownData && this.filteredComparisionData && this.filteredRows;
  }

  public renderBreakdownChart() {
    return this.schemeService.getAdminCostAndType(this.scheme.id).then((adminCostAndTypes: AdminCostAndType[]) => {
      this.adminCostAndTypes = adminCostAndTypes;

      const admin_cost_and_type = {adminCostAndType: this.adminCostAndTypes};
      this.ngRedux.dispatch({type: ADD_ADMIN_COST_AND_TYPE, admin_cost_and_type: admin_cost_and_type});

      this.breakdownData = {
        labels: this.adminCostAndTypes.map(adminCostAndType => adminCostAndType.type),
        original: merge({data: adminCostAndTypes}, {}).data,
        datasets: [{
          data: [...this.adminCostAndTypes.map(adminCostAndType => adminCostAndType.amount)],
          formatted: [...this.adminCostAndTypes.map(adminCostAndType => Utils.humanize(adminCostAndType.amount))],
          backgroundColor: ['#114D64', 'orange', 'dodgerblue', '#176C89', '#8E8E80', '#C2A878', '#30343F'],
        }],
      };
    })
      .then(this.filterBreakdownData.bind(this))
      .then(this.renderBreakdownTable.bind(this))
      .then(this.renderUniverseComparisionChart.bind(this));
  }

  public renderBreakdownTable() {
    this.rows = this.adminCostAndTypes.map(cost => {
      return {
        category: cost.type,
        amount: cost.amount,
        formattedAmount: Utils.wholeNum(cost.amount),
      };
    });
    this.filterBreakdownTable();
  }

  public renderUniverseComparisionChart() {
    this.schemeService.getUniverseAdminCostTypeAndNetAssets(this.scheme.id)
      .then((adminCostTypesAndNetAssets: AdminCostTypesAndNetAsset[]) => {
        this.adminCostTypesAndNetAssets = adminCostTypesAndNetAssets;
        const adminCostTypesAndNetAsset = new AdminCostTypesAndNetAsset();
        adminCostTypesAndNetAsset.net_assets = this.totalCosts.net_asset_amount;
        adminCostTypesAndNetAsset.adminCostAndTypes = this.adminCostAndTypes;
        adminCostTypesAndNetAssets.unshift(adminCostTypesAndNetAsset);

        function generateTitle(costTypes: AdminCostAndType[]) {
          const costTypeTotals = costTypes.map(c => c.amount);
          const costTotal = costTypeTotals.reduce((t, c) => t + c);
          const mv = costTotal;
          return 'Administrative Costs: ' + Utils.humanize(mv);
        }

        function generateLabel(costTypes: AdminCostAndType[]) {
          return costTypes.map(c => `${c.type}: ${Utils.humanize(c.amount)}`);
        }

        const datasets = this.adminCostTypesAndNetAssets.map((costTypesAndNetAsset) => {
          const total_admin_costs = costTypesAndNetAsset.adminCostAndTypes
            .map(cost => cost.amount)
            .reduce((total, cost) => total + cost);
          const net_assets = costTypesAndNetAsset.net_assets;

          return {
            pointRadius: 8,
            pointHoverRadius: 10,
            title: generateTitle(costTypesAndNetAsset.adminCostAndTypes),
            label: generateLabel(costTypesAndNetAsset.adminCostAndTypes),
            footer: 'Net Assets: ' + Utils.humanize(costTypesAndNetAsset.net_assets),
            original: merge({...costTypesAndNetAsset}, {}),
            data: [{
              x: net_assets,
              y: total_admin_costs,
            }]
          };
        });

        datasets[0]['backgroundColor'] = '#176C89';

        const filteredDataset = datasets.filter(ds => ds.data[0].y < ds.data[0].x);

        this.comparisionData = {
          datasets: [...filteredDataset],
        };
      }).then(this.filterComparisionData.bind(this));
  }

  get totalCost(): string {
    return Utils.humanize(this.totalCosts.total_cost);
  }

  get adminCost(): string {
    return Utils.humanize(this.totalCosts.admin_cost);
  }

  get adminCostBPS(): any {
    const bps = Math.fround((this.totalCosts.admin_cost / this.totalCosts.net_asset_amount) * 10000);
    return Utils.formatNumber(bps, '0a');
  }

  filterByCostType(event) {
    this.selectedCostTypes = event.value;
    this.filterBreakdownData();
    this.filterComparisionData();
    this.filterBreakdownTable();
  }

  filterByUnitType(event) {
    this.selectedUnit = event.value;
    this.filterBreakdownData();
    this.filterComparisionData();
    this.filterBreakdownTable();
  }

  private filterBreakdownData() {
    const data = merge({...this.breakdownData}, {});
    const net_asset_amount = this.totalCosts.net_asset_amount ? this.totalCosts.net_asset_amount : 0;

    this.filteredBreakdownData = this.breakdownFilters.reduce((d, filter) => filter(d, {
      net_assets: net_asset_amount,
      selectedUnit: this.selectedUnit,
      selectedCostTypes: this.selectedCostTypes,
    }), data);
  }

  private filterComparisionData() {
    const data = merge({...this.comparisionData}, {});

    this.filteredComparisionData = this.comparisionFilters.reduce((d, filter) => filter(d, {
      label: 'Administrative Costs',
      selectedUnit: this.selectedUnit,
      selectedCostTypes: this.selectedCostTypes,
    }), data);
  }

  private filterBreakdownTable() {
    const data = merge({data: [...this.rows]}, {});
    const net_asset_amount = this.totalCosts.net_asset_amount ? this.totalCosts.net_asset_amount : 0;

    this.filteredRows = this.tableFilters.reduce((d, filter) => {
      return filter(d, {
        net_assets: net_asset_amount,
        selectedUnit: this.selectedUnit,
        selectedCostTypes: this.selectedCostTypes,
      });
    }, data.data);

    this.calculateTotalFilteredAmount();
  }

  private calculateTotalFilteredAmount() {
    const total = this.filteredRows.reduce((sum, r) => {
      return sum + r.amount;
    }, 0);
    this.totalFilteredAmount = Utils.labelForUnit(
      Math.round(total),
      this.selectedUnit,
      Utils.defaultFormatForUnit(this.selectedUnit));
  }

}
