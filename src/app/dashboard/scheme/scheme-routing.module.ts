import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SchemeComponent} from './scheme.component';
import {IndexComponent} from './index/index.component';
import {PensionSchemeResolver} from '../../routes/resolves/PensionSchemeResolver';
import {AdminCostComponent} from './admin-cost/admin-cost.component';
import {AuxCostComponent} from './auxiliary-cost/auxiliary-cost.component';
import {ManagementCostComponent} from './management-cost/management-cost.component';
import {IntegratedReportComponent} from './performance/integrated-report.component';
import {DeductCostComponent} from './deduct-cost/deduct_cost.component';

import {PerformanceComponent} from './performances/performance.component';

import {InvestmentCostComponent} from './investment-cost/investment-cost.component';
import {RecurringCostComponent} from './recurring-cost/recurring-cost.component';
import {AssetAllocationComponent} from './asset-allocation/asset-allocation.component';

const routes: Routes = [
  {
    path: '',
    component: SchemeComponent,
    resolve: {
      pensionScheme: PensionSchemeResolver,
    },
    children: [
      {
        path: '',
        component: IndexComponent,
      },
      {
        path: 'admin-cost',
        component: AdminCostComponent,
      },
      {
        path: 'investment-cost',
        component: InvestmentCostComponent,
      },
      {
        path: 'auxiliary-cost',
        component: AuxCostComponent,
      },
      {
        path: 'investment-management-cost',
        component: ManagementCostComponent,
      },
      {
        path: 'recurring-cost',
        component: RecurringCostComponent,
      },
      {
        path: 'integrated-reporting',
        component: IntegratedReportComponent,
      },
      {
        path: 'deduct-cost',
        component: DeductCostComponent,
      },
      {
        path: 'asset-allocation',
        component: AssetAllocationComponent,
      },
      {
        path: 'performance',
        component: PerformanceComponent,
      },
      // {
      //   path: 'asset-allocation',
      //   component: AssetAllocationComponent,
      // },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    PensionSchemeResolver,
  ],
})
export class SchemeRoutingModule {
}
