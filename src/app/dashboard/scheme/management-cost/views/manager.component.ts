import {Component, Input, ViewChild} from '@angular/core';
import {InvestmentCostType} from '../../../../core/models/investment/InvestmentCostType';
import {Utils} from '../../../../utils/Utils';

@Component({
  selector: 'app-management-cost-manager-view',
  templateUrl: './manager.component.html',
})
export class ManagementManagerComponent {

  @Input() public filteredRows = [];

  @Input() public selectedUnit;

  @Input() public netAssets;

  @Input() public filteredBreakdownData = [];

  @Input() public filteredComparisionData = [];

  @Input() public costTypes: InvestmentCostType[];

  @Input() public calculateCostForMandateCostType: any;

  @Input() public calculateCostForManagerCostType: any;

  public expanded: any = {};

  @ViewChild('managementTable') private table: any;

  toggleDetail(event) {
    //
  }

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  formatTotal(total, parentTotal) {
    if (this.selectedUnit === 'bps') {
      return Utils.labelForUnit(Utils.calculateBPS(total, this.netAssets), this.selectedUnit, '0.0');
    }

    if (this.selectedUnit === 'percent') {
      return Utils.labelForUnit((total / parentTotal) * 100, this.selectedUnit, '0,0');
    }

    return Utils.labelForUnit(total, this.selectedUnit, '0,0');
  }

  format(value: number, format = '0,0') {
    return Utils.labelForUnit(value, this.selectedUnit, format);
  }

}
