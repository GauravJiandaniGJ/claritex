import {Component, Input, ViewChild} from '@angular/core';
import {ManagementCostType} from '../../../../core/models/management/ManagementCostType';
import {Utils} from '../../../../utils/Utils';

@Component({
  selector: 'app-management-cost-type-view',
  templateUrl: './cost-type.component.html',
})
export class ManagementCostTypeComponent {

  @Input() public filteredRows = [];

  @Input() public filteredBreakdownData = [];

  @Input() public filteredMandates = [];

  @Input() public filteredComparisionData = [];

  @Input() public selectedUnit = '';

  @Input() public costTypes: ManagementCostType[];

  @Input() public calculateCostForCostType;

  @Input() public calculateCostForMandateCostType;

  @Input() public calculateCostForManagerCostType: any;

  public expanded: any = {};

  @ViewChild('managementTable') private table: any;

  toggleDetail(event) {
    //
  }

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  format(value: number, format = '0,0') {
    return Utils.labelForUnit(value, this.selectedUnit, format);
  }

}
