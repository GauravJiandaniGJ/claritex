import {Component, Input, ViewChild} from '@angular/core';
import {InvestmentCostType} from '../../../../core/models/investment/InvestmentCostType';
import {Utils} from '../../../../utils/Utils';

@Component({
  selector: 'app-management-cost-mandate-view',
  templateUrl: './mandate.component.html',
})
export class ManagementMandateComponent {

  @Input() public filteredRows = [];

  @Input() public costTypes: InvestmentCostType[];

  @Input() public selectedUnit;

  @Input() public netAssets;

  @Input() public filteredBreakdownData = [];

  @Input() public filteredComparisionData = [];

  @Input() public calculateCostForMandateCostType;

  @Input() public calculateCostForManagerCostType: any;

  public expanded: any = {};

  @ViewChild('managementTable') private table: any;

  toggleDetail(event) {
    //
  }

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  format(value: number, format = '0,0') {
    return Utils.labelForUnit(value, this.selectedUnit, format);
  }

  formatTotalForRow(row, rows) {
    if (this.selectedUnit === 'bps') {
      return Utils.labelForUnit(Utils.calculateBPS(row.amount, this.netAssets), this.selectedUnit, '0.0');
    }

    if (this.selectedUnit === 'percent') {
      const parentTotal = rows.reduce((s, r) => s + r.amount, 0);
      return Utils.labelForUnit((row.amount / parentTotal) * 100, this.selectedUnit, '0,0');
    }

    return Utils.labelForUnit(row.amount, this.selectedUnit, '0,0');
  }

}
