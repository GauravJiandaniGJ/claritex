import {Component, OnInit, ViewChild} from '@angular/core';
import {PensionScheme} from '../../../core/models/PensionScheme';
import {ActivatedRoute} from '@angular/router';
import {SchemeService} from '../../../core/services/scheme.service';
import {PensionSchemeTotalCosts} from '../../../core/models/PensionSchemeTotalCosts';
import merge from 'deepmerge';
import {UnitTypeFilter} from '../../../filters/unit-type/unit-type-filter';
import {ManagementCostType} from '../../../core/models/management/ManagementCostType';
import {ManagementCostTypeAsset} from '../../../core/models/management/ManagementCostTypeAsset';
import {AssetManager} from '../../../core/models/AssetManager';
import {Mandate} from '../../../core/models/Mandate';
import {MandateService} from '../../../core/services/mandate.service';
import {Utils} from '../../../utils/Utils';
import {AssetClass} from '../../../core/models/AssetClass';
import {Cost} from '../../../core/models/Cost';
import {CostTreeCalculator} from '../../../core/models/CostTreeCalculator';

class ManagementFilters {
  managerFilter: Function = (o) => o;
  mandateFilter: Function = (o) => o;
  categoryFilter: Function = (o) => o;
  typeFilter: Function = (o) => o;
  assetFilter: Function = (o) => o;
}

@Component({
  selector: 'app-management-cost',
  templateUrl: './recurring-cost.component.html',
})
export class RecurringCostComponent implements OnInit {

  scheme: PensionScheme;

  assetClasses: AssetClass[];

  managers: AssetManager[];

  mandates: Mandate[];

  selectedUnit = 'gbp';

  filteredRows = [];

  columns = [];

  selectedManagers = [];

  selectedMandates = [];

  selectedAssetClasses = [];

  selectedCostTypes = [];

  costTypes: ManagementCostType[];

  filteredBreakdownData;

  filteredMandates: Cost[] = [];

  options = {};

  filteredComparisionData;

  expanded: any = {};

  views = [];

  selectedView: any;

  filteredCostTypes: ManagementCostType[];

  @ViewChild('managementTable') table: any;

  private comparisionData;

  private rows = [];

  private breakdownData: any;

  public totalCosts: PensionSchemeTotalCosts;

  private breakdownFilters: Array<Function> = [];

  private managementCosts: Cost;

  private filteredManagementCosts: Cost;

  private defaultFilter: ManagementFilters;

  private tableFilters: Array<Function> = [];

  private comparisionFilters: Array<Function> = [];

  private costTypeTotals: ManagementCostType[] = [];

  public filteredMandateBreakdown: any;

  private universeManagementCosts: Cost[];

  private filteredUniverseManagementCost: any;

  private filteredAssetClasses: AssetClass[];

  filteredCostTypesBreakdown: any;

  private RECURRING_COST_TYPE_ID = 17;

  constructor(private route: ActivatedRoute, private schemeService: SchemeService,
              private mandateService: MandateService, private unitTypeFilter: UnitTypeFilter) {

    this.columns = [
      {name: 'Asset Manager', prop: 'manager'},
      {name: 'Total', prop: 'total'},
      {name: 'Total (Formatted)', prop: 'formattedTotal'},
    ];

    this.views = [
      {
        label: 'View By Manager',
        value: 'asset_manager',
      },
      {
        label: 'View By Mandate',
        value: 'mandate',
      },
      {
        label: 'View By Cost Type',
        value: 'cost_type',
      }
    ];

    this.selectedView = 'asset_manager';

    this.options = this.chartOptions();

    this.makeDefaultFilter();
    this.breakdownFilters.push(unitTypeFilter.forBreakdown);
    this.tableFilters.push(unitTypeFilter.forManagementTable);
    this.comparisionFilters.push(unitTypeFilter.forComparision);
  }

  ngOnInit() {
    this.route.parent.data.subscribe((data: { pensionScheme: PensionScheme }) => {
      this.scheme = data.pensionScheme;

      this.schemeService.getTotalCosts(this.scheme.id).then((tc: PensionSchemeTotalCosts) => {
        this.totalCosts = tc;
      })
        .then(this.fetchCostTypes.bind(this))
        .then(this.fetchAssetClasses.bind(this))
        .then(this.fetchManagementCosts.bind(this))
        .then(this.fetchUniverseCost.bind(this));
    });
  }

  public get pageLoaded() {
    return this.scheme && this.filteredBreakdownData && this.filteredRows && this.filteredUniverseManagementCost;
  }

  public get rootCostTypes(): ManagementCostType[] {
    return this.costTypes;
  }

  public fetchCostTypes() {
    return this.mandateService.managementCostTypes()
      .then(cts => {
        this.costTypes = cts.all().filter(ct => ct.parent_id === this.RECURRING_COST_TYPE_ID);
        this.filteredCostTypes = this.costTypes;
      });
  }

  public fetchAssetClasses() {
    return this.mandateService.allAssetClasses()
      .then(collection => {
        this.assetClasses = collection.all();
        this.filteredAssetClasses = this.assetClasses;
      });
  }

  public fetchManagementCosts() {
    return this.schemeService.getManagementCosts(this.scheme.id, this.RECURRING_COST_TYPE_ID).then((costs: Cost) => {
      this.managementCosts = costs;
      this.managementCosts.breakdown.forEach(manager => {
        manager.breakdown.forEach(mandate => {
          mandate.breakdown = mandate.breakdown[0].breakdown;
        });
      });

      this.costTypes = merge({d: []}, {d: costs.breakdown[0].breakdown[0].breakdown}).d;
      CostTreeCalculator.calculate(costs);
      this.managementCosts.amount = costs.amount;
    })
      .then(() => this.managers = merge({d: [...this.managementCosts.breakdown]}, {}).d)
      .then(() => {
        this.mandates = [].concat.apply([], this.managementCosts.breakdown.map(m => m.breakdown));
      })
      .then(this.prepareData.bind(this))
      .then(this.renderBreakdownTable.bind(this));
  }

  public fetchUniverseCost() {
    return this.schemeService.getUniverseManagementCosts(this.scheme.id, this.RECURRING_COST_TYPE_ID)
      .then((costs: Cost) => {
        costs.breakdown.forEach(m => {
          m.breakdown.forEach(manager => {
            manager.breakdown.forEach(mandate => {
              mandate.breakdown = mandate.breakdown[0].breakdown;
            });
          });
        });

        // Add current pension scheme to the comparision data
        const pensionScheme = merge({...this.managementCosts}, {});
        pensionScheme.info = {net_asset_amount: this.totalCosts.net_asset_amount};
        pensionScheme.is_current_scheme = true;
        costs.breakdown.unshift(pensionScheme);
        this.universeManagementCosts = costs.breakdown;
      })
      .then(this.prepareComparisionData.bind(this))
      .then(this.renderComparisionChart.bind(this));
  }

  public prepareData() {
    const allManagementCosts = merge({...this.managementCosts}, {});
    const filter = {...this.defaultFilter};
    filter.typeFilter = (ct) => ct;
    allManagementCosts.breakdown = this.filterData(allManagementCosts.breakdown, filter);
    CostTreeCalculator.calculate(allManagementCosts);
    this.filteredManagementCosts = allManagementCosts;
    const costTypes = this.selectedCostTypes.length ? this.selectedCostTypes : this.costTypes;
    this.costTypeTotals = this.calculateCostTypeTotals(merge({d: costTypes}, {}).d);
  }

  public prepareComparisionData() {
    const universeManagementCosts = merge({d: this.universeManagementCosts}, {}).d;
    const filter = merge({...this.defaultFilter}, {});
    filter.mandateFilter = (m) => m;
    this.filteredUniverseManagementCost = universeManagementCosts.map(uc => {
      uc.breakdown = this.filterData(uc.breakdown, filter);
      CostTreeCalculator.calculate(uc);
      return uc;
    }).filter(uc => uc.breakdown.length);
  }

  public renderBreakdownTable() {
    this.rows = this.filteredManagementCosts.breakdown.map(manager => {
      const categoryValues = this.calculateCostTypeTotals(this.costTypeTotals, manager.id).reduce((costTypes, c) => {
        let k = `cat_${c.id}`;
        costTypes[k] = c.total;
        k += '_filtered';
        costTypes[k] = c.total;
        return costTypes;
      }, {});

      return {
        id: manager.id,
        manager: manager,
        name: manager.name,
        total: manager.amount,
        formattedTotal: Utils.wholeNum(manager.amount),
        ...categoryValues,
      };
    });
    this.filterBreakdownTable();
    this.renderBreakdownChart();
  }

  public renderBreakdownChart() {
    const managers = this.filteredRows.map(r => r.manager);

    this.breakdownData = {
      labels: [...managers.map(m => m.name)],
      datasets: [{
        data: [...managers.map(c => c.amount)],
        formatted: [...managers.map(c => Utils.labelForUnit(c.amount, this.selectedUnit, '0.[0]a'))],
        backgroundColor: Utils.randomColor({
          count: managers.length,
          alpha: '0.4',
          luminosity: 'dark',
        }),
      }],
    };
    this.filterBreakdownData();
  }

  public renderComparisionChart() {
    const datasets = this.filteredUniverseManagementCost.map((managementCost) => {
      const net_assets = managementCost.info.net_asset_amount;
      const investmentCost = managementCost.amount;

      const ds = {
        pointRadius: 8,
        pointHoverRadius: 10,
        label: [`Recurring Costs: ${Utils.humanize(investmentCost)}`, `Net Assets: ${Utils.humanize(net_assets)}`],
        original: merge({...managementCost}, {}),
        data: [{
          y: investmentCost,
          x: net_assets,
        }]
      };

      if (managementCost.is_current_scheme) {
        ds['backgroundColor'] = '#176C89';
      }

      return ds;
    });

    this.comparisionData = {
      datasets: merge({d: datasets}, {}).d,
    };

    this.filterComparisionData();
  }

  filterByAssetManager(event) {
    this.selectedManagers = event.value;

    if (this.selectedManagers.length) {
      this.mandates = [].concat.apply([], this.selectedManagers.map(m => m.breakdown));
    } else {
      this.mandates = [].concat.apply([], this.managementCosts.breakdown.map(m => m.breakdown));
    }

    this.prepareData();
    this.renderBreakdownTable();
    this.prepareComparisionData();
    this.renderComparisionChart();
  }

  filterByMandate(event) {
    this.selectedMandates = event.value;
    this.prepareData();
    this.renderBreakdownTable();
    this.renderComparisionChart();
  }

  filterByCostType(event) {
    this.selectedCostTypes = event.value;

    if (this.selectedCostTypes.length) {
      this.filteredCostTypes = this.costTypes.filter(
        ct => {
          return this.selectedCostTypes
            .some(cat => cat.id === ct.id);
        }
      );
    } else {
      this.filteredCostTypes = [...this.costTypes];
      this.filteredAssetClasses = [...this.assetClasses];
    }

    this.prepareData();
    this.renderBreakdownTable();
    this.prepareComparisionData();
    this.renderComparisionChart();
  }

  filterByAssetClass(event) {
    this.selectedAssetClasses = event.value;
    this.prepareData();
    this.renderBreakdownTable();
    this.prepareComparisionData();
    this.renderComparisionChart();
  }

  filterByUnitType(event) {
    this.selectedUnit = event.value;
    this.prepareData();
    this.renderBreakdownTable();
    this.prepareComparisionData();
    this.renderComparisionChart();
  }

  private filterBreakdownData() {
    const data = merge({...this.breakdownData}, {});
    const net_asset_amount = this.totalCosts.net_asset_amount ? this.totalCosts.net_asset_amount : 0;

    this.filteredBreakdownData = this.breakdownFilters.reduce((d, filter) => filter(d, {
      net_assets: net_asset_amount,
      selectedUnit: this.selectedUnit,
      selectedCostTypes: this.selectedCostTypes,
    }), data);
  }

  private filterBreakdownTable() {
    this.rows = this.rows.filter(function (m) {
      if (m.total !== 0) {
        return m;
      }
    });

    const data = merge({data: [...this.rows]}, {});
    const net_asset_amount = this.totalCosts.net_asset_amount ? this.totalCosts.net_asset_amount : 0;

    this.filteredRows = this.tableFilters.reduce((d, filter) => {
      return filter(d, {
        net_assets: net_asset_amount,
        selectedUnit: this.selectedUnit,
        selectedCostTypes: this.selectedCostTypes,
      });
    }, data.data);
    this.filteredMandates = this.filteredRows.reduce((m, row) => m.concat(row.manager.breakdown), []);
    this.filteredMandateBreakdown = {
      labels: [...this.filteredMandates.map(m => m.name)],
      datasets: [{
        data: [...this.filteredMandates.map(c => c.amount)],
        formatted: [...this.filteredMandates.map(c => c.amount)],
        backgroundColor: Utils.randomColor({
          count: this.filteredMandates.length,
          alpha: '0.4',
          luminosity: 'dark',
        }),
      }],
    };
    const costTypes = this.selectedCostTypes.length ? this.selectedCostTypes : this.costTypes;
    this.filteredCostTypes = costTypes.map(cat => {
      const value = this.filteredMandates.reduce((sum, mandate) => {
        const mandateCost = mandate.breakdown.find(c => c.id === cat.id);
        const cost = mandateCost != null ? mandateCost.amount : 0;
        return sum + cost;
      }, 0);
      const total = value;
      cat.total = total;
      return cat;
    });

    this.filteredCostTypesBreakdown = {
      labels: [...this.filteredCostTypes.map(m => m.name)],
      datasets: [{
        data: [...this.filteredCostTypes.map(c => c.total)],
        formatted: [...this.filteredCostTypes.map(c => c.total)],
        backgroundColor: Utils.randomColor({
          count: this.filteredCostTypes.length,
          alpha: '0.4',
          luminosity: 'dark',
        }),
      }],
    };
  }

  private filterComparisionData() {
    const data = merge({...this.comparisionData}, {});

    this.filteredComparisionData = this.comparisionFilters.reduce((d, filter) => filter(d, {
      label: 'Universe Comparision - Recurring Costs',
      selectedUnit: this.selectedUnit,
      selectedCostTypes: this.selectedCostTypes,
    }), data);
  }

  private filterData(managers: Cost[], filters?: ManagementFilters) {
    if (!filters) {
      filters = new ManagementFilters();
    }

    filters = merge(new ManagementFilters(), filters);

    return managers.filter(m => {
      m.breakdown = m.breakdown.filter(mc => {
        mc.breakdown = this.filterCostTypesRecursively(mc.breakdown, filters);
        // Filter Mandate Costs
        return this.filterMandates(mc, filters.mandateFilter);
      });
      // Filter Managers
      return this.filterManagers(m, filters.managerFilter);
    });
  }

  private filterManagers(m: Cost, filter?: Function): boolean {
    if (filter) {
      return filter(m);
    }
    return true;
  }

  private filterMandates(mc: Cost, filter?: Function): boolean {
    if (filter) {
      return filter(mc);
    }
    return true;
  }

  private filterCostTypes(tc: ManagementCostType | Cost, filter?: Function): boolean {
    if (filter) {
      return filter(tc);
    }
    return true;
  }

  private filterAssetClasses(ac: Cost | ManagementCostTypeAsset, filter?: Function): boolean {
    if (filter) {
      return filter(ac);
    }
    return true;
  }

  private filterForCostType(cat: number): ManagementFilters {
    const filter = new ManagementFilters();
    filter.typeFilter = (c => c.id === cat);
    return filter;
  }

  private filterForManagerAndCostType(cat: number, man: number): ManagementFilters {
    const filter = new ManagementFilters();
    filter.typeFilter = (c => c.id === cat);
    filter.managerFilter = (m => m.id === man);
    return filter;
  }

  private calculateTotalForManagerMandateAndCostType(cat: ManagementCostType, man: number, mandate: number) {
    const managementCost = merge({...this.filteredManagementCosts}, {});
    managementCost.breakdown = this
      .filterData(managementCost.breakdown, this.filterForManagerMandateAndCostType(cat.id, man, mandate));
    CostTreeCalculator.calculate(managementCost);
    cat.total = managementCost.amount;
    return cat;
  }

  private filterForManagerMandateAndCostType(cat: number, man: number, mad: number): ManagementFilters {
    const filter = new ManagementFilters();
    filter.categoryFilter = (c => c.id === cat);
    filter.managerFilter = (m => m.id === man);
    filter.mandateFilter = (m => m.id === man);
    return filter;
  }

  private calculateCostTypeTotals(costTypes: ManagementCostType[], manager_id?: number) {
    return costTypes.map(cat => {
      if (manager_id) {
        return this.calculateTotalForManagerAndCostType(cat, manager_id);
      }
      return this.calculateTotalForCostType(cat);
    });
  }

  private calculateTotalForCostType(cat: ManagementCostType) {
    const managementCost = merge({...this.filteredManagementCosts}, {});
    managementCost.breakdown = this.filterData(managementCost.breakdown, this.filterForCostType(cat.id));
    CostTreeCalculator.calculate(managementCost);
    cat.total = managementCost.amount;
    return cat;
  }

  private calculateTotalForManagerAndCostType(cat: ManagementCostType, man: number) {
    const managementCost = merge({...this.filteredManagementCosts}, {});
    managementCost.breakdown = this.filterData(managementCost.breakdown, this.filterForManagerAndCostType(cat.id, man));
    CostTreeCalculator.calculate(managementCost);
    cat.total = managementCost.amount;
    return cat;
  }

  private makeDefaultFilter() {
    const filter = new ManagementFilters();
    filter.categoryFilter = (m => {
      if (!this.selectedCostTypes.length) {
        return true;
      }
      return this.selectedCostTypes.some(mm => m.id === mm.id);
    });
    filter.managerFilter = (m => {
      if (!this.selectedManagers.length) {
        return true;
      }
      return this.selectedManagers.some(mm => m.id === mm.id);
    });
    filter.mandateFilter = (m => {
      if (!this.selectedMandates.length) {
        return true;
      }
      return this.selectedMandates.some(mm => m.id === mm.id);
    });
    filter.typeFilter = (m => {
      if (!this.selectedCostTypes.length) {
        return true;
      }
      return this.selectedCostTypes.some(mm => m.id === mm.id);
    });
    filter.assetFilter = (m => {
      if (!this.selectedAssetClasses.length) {
        return true;
      }
      return this.selectedAssetClasses.some(mm => m.id === mm.id);
    });

    this.defaultFilter = filter;
  }

  chartOptions() {
    return {
      //
    };
  }

  toggleDetail(event) {
    //
  }

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  formatCurrency(c) {
    return Utils.wholeNum(c);
  }

  findCostTypeRecursive(id: number, costType: Cost): Cost {
    if (costType.id === id) {
      return costType;
    }

    let f = null;

    if (costType.breakdown && costType.breakdown.length) {
      costType.breakdown.forEach(ct => {
        const found = this.findCostTypeRecursive(id, ct);

        if (found) {
          f = found;
        }
      });

      return f;
    }

    return null;
  }

  calculateCostForMandateCostType(mandate, costType) {
    if (!mandate) {
      return 0;
    }

    let costTypeCost;

    mandate.breakdown.forEach(ct => {
      const found = this.findCostTypeRecursive(costType, ct);
      if (found) {
        costTypeCost = found;
      }
    });

    if (!costTypeCost) {
      return 0;
    }

    const total = costTypeCost.amount;

    if (this.selectedUnit === 'gbp') {
      return Utils.wholeNum(total);
    }

    if (this.selectedUnit === 'bps') {
      const bps = Utils.calculateBPS(total, this.totalCosts.net_asset_amount);
      return Utils.formatNumber(bps, '0.0a') + ' BPS';
    }

    const percent = ((total / this.totalCosts.net_asset_amount) * 100);
    return Utils.formatNumber(percent, '0.0a') + '%';
  }

  calculateCostForCostType(mandates, costType) {

    if (!mandates.length) {
      return 0;
    }

    let costTypeCost;

    const number = mandates.reduce((sum, mandate) => {
      mandate.breakdown.forEach(ct => {
        const found = this.findCostTypeRecursive(costType, ct);
        if (found) {
          costTypeCost = found;
        }
      });

      if (!costTypeCost) {
        return 0;
      }

      return sum + costTypeCost.amount;
    }, 0);

    if (this.selectedUnit === 'gbp') {
      return Utils.wholeNum(number);
    }

    if (this.selectedUnit === 'bps') {
      const bps = Utils.calculateBPS(number, this.totalCosts.net_asset_amount);
      return Utils.formatNumber(bps, '0.0a') + ' BPS';
    }

    const percent = ((number / this.totalCosts.net_asset_amount) * 100);
    return Utils.formatNumber(percent, '0.0a') + '%';
  }

  private filterCostTypesRecursively(costTypes: Cost[], filters: ManagementFilters) {
    return costTypes.filter(ct => {

      if (ct.breakdown && ct.breakdown.length) {
        ct.breakdown = this.filterCostTypesRecursively(ct.breakdown, filters);
      }

      if (ct.breakdown && ct.breakdown.length) {
        ct.breakdown = ct.breakdown.filter(ac => {
          // Filter Asset Costs
          return this.filterAssetClasses(ac, filters.assetFilter);
        });
      }
      // Filter Type Costs
      return this.filterCostTypes(ct, filters.typeFilter);
    });
  }

  calculateCostForManagerCostType(manager, costType) {
    if (!manager) {
      return 0;
    }

    const total = manager.breakdown.reduce((sum, mandate) => {
      const costTypeCost = mandate.breakdown.find(c => c.id === costType);

      if (!costTypeCost) {
        return sum;
      }

      return sum + costTypeCost.amount;
    }, 0);

    if (this.selectedUnit === 'gbp') {
      return Utils.wholeNum(total);
    }

    if (this.selectedUnit === 'bps') {
      const bps = Utils.calculateBPS(total, this.totalCosts.net_asset_amount);
      return Utils.formatNumber(bps, '0.0a') + ' BPS';
    }

    const percent = ((total / this.totalCosts.net_asset_amount) * 100);
    return Utils.formatNumber(percent, '0.0a') + '%';
  }

}
