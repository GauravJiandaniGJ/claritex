import {Component, Input, ViewChild} from '@angular/core';
import {ManagementCostType} from '../../../../core/models/management/ManagementCostType';
import {Utils} from '../../../../utils/Utils';

@Component({
  selector: 'app-recurring-cost-type-view',
  templateUrl: './cost-type.component.html',
})
export class RecurringCostTypeComponent {

  @Input() public filteredRows = [];

  @Input() public filteredMandates = [];

  @Input() public selectedUnit;

  @Input() public netAssets;

  @Input() public filteredBreakdownData = [];

  @Input() public filteredComparisionData = [];

  @Input() public costTypes: ManagementCostType[];

  @Input() public calculateCostForCostType;

  @Input() public calculateCostForMandateCostType;

  @Input() public calculateCostForManagerCostType;

  public expanded: any = {};

  @ViewChild('managementTable') private table: any;

  toggleDetail(event) {
    //
  }

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  formatTotalForRow(row, rows) {
    if (this.selectedUnit === 'bps') {
      return Utils.labelForUnit(Utils.calculateBPS(row.total, this.netAssets), this.selectedUnit, '0.0');
    }

    if (this.selectedUnit === 'percent') {
      const parentTotal = rows.reduce((s, r) => s + r.total, 0);
      return Utils.labelForUnit((row.total / parentTotal) * 100, this.selectedUnit, '0,0');
    }

    return Utils.labelForUnit(row.total, this.selectedUnit, '0,0');
  }

  format(value: number, format = '0,0') {
    return Utils.labelForUnit(value, this.selectedUnit, format);
  }

}
