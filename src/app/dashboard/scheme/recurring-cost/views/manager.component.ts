import {Component, Input, ViewChild} from '@angular/core';
import {InvestmentCostType} from '../../../../core/models/investment/InvestmentCostType';
import {Utils} from '../../../../utils/Utils';

@Component({
  selector: 'app-recurring-cost-manager-view',
  templateUrl: './manager.component.html',
})
export class RecurringManagerComponent {

  @Input() public filteredRows = [];

  @Input() public selectedUnit;

  @Input() public netAssets;

  @Input() public filteredBreakdownData = [];

  @Input() public filteredComparisionData = [];

  @Input() public costTypes: InvestmentCostType[];

  @Input() public calculateCostForMandateCostType: any;

  @Input() public calculateCostForManagerCostType;

  public expanded: any = {};

  @ViewChild('managementTable') private table: any;

  toggleDetail(event) {
    //
  }

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  formatTotal(total, parentTotal) {
    if (this.selectedUnit === 'bps') {
      return Utils.labelForUnit(Utils.calculateBPS(total, this.netAssets), this.selectedUnit, '0.0');
    }

    if (this.selectedUnit === 'percent') {
      return Utils.labelForUnit((total / parentTotal) * 100, this.selectedUnit, '0,0');
    }

    return Utils.labelForUnit(total, this.selectedUnit, '0,0');
  }

  formatTotalForRow(row, rows) {
    if (this.selectedUnit === 'bps') {
      return Utils.labelForUnit(Utils.calculateBPS(row.amount, this.netAssets), this.selectedUnit, '0.0');
    }

    if (this.selectedUnit === 'percent') {
      const parentTotal = rows.reduce((s, r) => s + r.amount, 0);
      return Utils.labelForUnit((row.amount / parentTotal) * 100, this.selectedUnit, '0,0');
    }

    return Utils.labelForUnit(row.amount, this.selectedUnit, '0,0');
  }

}
