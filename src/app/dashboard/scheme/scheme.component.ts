import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PensionScheme} from '../../core/models/PensionScheme';
import {MenuItem} from '../../components/menu/MenuItem';
import {THEME_CONFIG} from '../../theme/theme-config';
import {WaterfallChart} from '../../core/charts/waterfall.chart';

@Component({
  selector: 'app-scheme',
  templateUrl: './scheme.component.html',
  styleUrls: ['./scheme.component.scss'],
})
export class SchemeComponent implements OnInit {

  scheme: PensionScheme;

  items: MenuItem[];

  constructor(private route: ActivatedRoute) {
    const config = THEME_CONFIG;
    const Chart = window['Chart'];
    const chartjs: any = config.variables.chartjs;
    SchemeComponent.setChartGlobalOptions(Chart, chartjs, config);
    SchemeComponent.registerWaterfallChart(Chart, chartjs);
  }

  static setChartGlobalOptions(Chart: any, chartConfig: any, config: any) {
    const chartGlobals = Chart.defaults.global;
    chartGlobals.maintainAspectRatio = true;
    chartGlobals.responsive = true;
    // chartGlobals.defaultFontSize = 12;
    chartGlobals.defaultFontFamily = config.variables.fontMain;
    chartGlobals.defaultColor = config.variables.fgText;
    chartGlobals.defaultFontColor = chartGlobals.defaultColor;
    chartGlobals.tooltips.cornerRadius = 2;
    chartGlobals.tooltips.xPadding = 10;
    chartGlobals.tooltips.bodySpacing = 10;
    chartGlobals.tooltips.footerMarginTop = 12;
    chartGlobals.tooltips.titleMarginBottom = 12;
    chartGlobals.tooltips.yPadding = 10;
    chartGlobals.legend.position = 'bottom';
  }

  static registerWaterfallChart(Chart: any, config: any) {
    WaterfallChart.register(Chart, config);
  }

  ngOnInit() {
    this.route.data.subscribe((data: { pensionScheme: PensionScheme }) => {
      this.scheme = data.pensionScheme;
      this.items = this.getItems();
    });
  }

  public getItems(): MenuItem[] {
    return [
      {
        title: 'Asset Allocation',
        link: `/dashboard/scheme/${this.scheme.id}/asset-allocation`,
        pathMatch: 'full',
      },
      {
        title: 'Total Costs',
        link: `/dashboard/scheme/${this.scheme.id}`,
        pathMatch: 'full',
      },
      {
        title: 'Administrative Costs',
        link: `/dashboard/scheme/${this.scheme.id}/admin-cost`,
      },
      {
        title: 'Investment Costs',
        expanded: false,
        link: `/dashboard/scheme/${this.scheme.id}/investment-cost`,
        children: [
          // {
          //   title: 'Overview',
          //   icon: 'fas fa-columns',
          //   link: `/dashboard/scheme/${this.scheme.id}/investment-cost`,
          //   parent: {
          //     title: 'Investment Costs',
          //     icon: 'fas fa-calculator',
          //     expanded: true,
          //   },
          // },
          {
            title: 'Auxiliary Service Costs',
            link: `/dashboard/scheme/${this.scheme.id}/auxiliary-cost`,
          },
          {
            title: 'Management Costs',
            link: `/dashboard/scheme/${this.scheme.id}/investment-management-cost`,
            children: [
              // {
              //   title: 'Overview',
              //   icon: 'fas fa-columns',
              //   link: `/dashboard/scheme/${this.scheme.id}/investment-management-cost`,
              // },
              {
                title: 'Recurring Costs',
                link: `/dashboard/scheme/${this.scheme.id}/recurring-cost`,
              },
            ]
          },
        ]
      },
      {
        title: 'Integrated Reporting',
        link: `/dashboard/scheme/${this.scheme.id}/integrated-reporting`,
      },
      {
        title: 'Performance',
        link: `/dashboard/scheme/${this.scheme.id}/performance`,
        pathMatch: 'full',
      },
    ];
  }
}
