import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PensionScheme} from '../../../core/models/PensionScheme';
import {SchemeService} from '../../../core/services/scheme.service';
import {PensionSchemeTotalCosts} from '../../../core/models/PensionSchemeTotalCosts';
import {UniverseTotalCostAndNetAsset} from '../../../core/models/UniverseTotalCostAndNetAsset';

import {Utils} from '../../../utils/Utils';
import merge from 'deepmerge';
import {UnitTypeFilter} from '../../../filters/unit-type/unit-type-filter';

import {CostService} from '../../../core/services/cost.service';
import {Cost} from '../../../core/models/Cost';
import {CostTreeCalculator} from '../../../core/models/CostTreeCalculator';


@Component({
  selector: 'app-index',
  templateUrl: './investment-cost.component.html',
  providers: [CostService]
})
export class InvestmentCostComponent implements OnInit {

  options: any;

  scheme: PensionScheme;

  totalCosts: PensionSchemeTotalCosts = null;

  selectedUnit = 'gbp';

  filteredBreakdownData: any;

  filteredComparisionData: any;

  private breakdownFilters: Array<Function> = [];

  private comparisionFilters: Array<Function> = [];

  private comparisionData: any;

  private breakdownData: any;

  cost: Cost;
  tot: any;

  constructor(private route: ActivatedRoute, private schemeService: SchemeService, private unitTypeFilter: UnitTypeFilter,
              private costService: CostService) {
    this.breakdownFilters.push(unitTypeFilter.forBreakdown);
    this.comparisionFilters.push(unitTypeFilter.forComparision);
  }

  ngOnInit() {
    this.route.parent.data.subscribe((data: { pensionScheme: PensionScheme }) => {
      this.scheme = data.pensionScheme;
      this.schemeService.getTotalCosts(this.scheme.id)
        .then(this.renderBreakdownChart.bind(this))
        .then(this.filterBreakdownData.bind(this))
        .then(this.renderUniverseComparisionChart.bind(this));
    });


    this.costService.getCost()
      .then(res => {
        this.cost = res;
        this.tot = CostTreeCalculator.calculate(this.cost);
        console.log(this.tot);
      });
  }


  public get pageLoaded() {
    return this.scheme && this.filteredBreakdownData && this.filteredComparisionData;
  }

  get investmentCost(): string {
    return Utils.humanize(this.totalCosts.investment_cost);
  }

  get investmentManagementCost(): string {
    return Utils.humanize(this.totalCosts.investment_management_cost);
  }

  get totalCost(): string {
    return Utils.humanize(this.totalCosts.total_cost);
  }

  get auxiliaryCost(): string {
    return Utils.humanize(this.totalCosts.investment_auxiliary_cost);
  }

  filterByUnitType(event) {
    this.selectedUnit = event.value;
    this.filterBreakdownData();
    this.filterComparisionData();
  }

  private generateLabel(totalCosts: number, netAssets: number) {
    return [`Total Costs: ${Utils.humanize(totalCosts)}`, `Net Assets: ${Utils.humanize(netAssets)}`];
  }

  private renderBreakdownChart(tc: PensionSchemeTotalCosts) {
    this.totalCosts = tc;
    this.breakdownData = {
      labels: ['Investment Management Costs', 'Auxiliary Service Costs'],
      datasets: [{
        data: [this.totalCosts.investment_management_cost, this.totalCosts.investment_auxiliary_cost],
        formatted: [
          Utils.humanize(this.totalCosts.investment_management_cost),
          Utils.humanize(this.totalCosts.investment_auxiliary_cost)
        ],
        backgroundColor: ['#176C89', '#114D64'],
      }],
    };
  }

  private renderUniverseComparisionChart() {
    this.schemeService
      .getUniverseInvestmentCosts(this.scheme.id)
      .then((universeTotalCostAndAssets: UniverseTotalCostAndNetAsset[]) => {
        const pensionSchemeCostAsset = new UniverseTotalCostAndNetAsset();
        pensionSchemeCostAsset.total_costs = this.totalCosts.investment_cost;
        pensionSchemeCostAsset.net_assets = this.totalCosts.net_asset_amount;
        universeTotalCostAndAssets.unshift(pensionSchemeCostAsset);

        const datasets = universeTotalCostAndAssets.map((universeTotalCostAndAsset) => {
          const total_costs = universeTotalCostAndAsset.total_costs;
          const net_assets = universeTotalCostAndAsset.net_assets;

          return {
            label: this.generateLabel(universeTotalCostAndAsset.total_costs, universeTotalCostAndAsset.net_assets),
            pointRadius: 8,
            pointHoverRadius: 10,
            data: [{
              x: net_assets,
              y: total_costs,
            }],
          };
        });

        datasets[0]['backgroundColor'] = '#176C89';

        const filteredDataset = datasets.filter(ds => ds.data[0].y < ds.data[0].x);

        this.comparisionData = {
          datasets: [...filteredDataset],
        };
      })
      .then(this.filterComparisionData.bind(this));
  }

  private filterBreakdownData() {
    const data = merge({...this.breakdownData}, {});
    const net_asset_amount = this.totalCosts.net_asset_amount ? this.totalCosts.net_asset_amount : 0;

    this.filteredBreakdownData = this.breakdownFilters.reduce((d, filter) => filter(d, {
      net_assets: net_asset_amount,
      selectedUnit: this.selectedUnit
    }), data);
  }

  private filterComparisionData() {
    const data = merge({...this.comparisionData}, {});

    this.filteredComparisionData = this.comparisionFilters.reduce((d, filter) => filter(d, {
      selectedUnit: this.selectedUnit
    }), data);
  }
}
