import {Component, OnInit} from '@angular/core';
import {PensionScheme} from '../../../core/models/PensionScheme';
import {ActivatedRoute} from '@angular/router';
import {AuxCostAndType} from '../../../core/models/AuxCostAndType';
import {SchemeService} from '../../../core/services/scheme.service';
import {AuxCostTypesAndNetAsset} from '../../../core/models/AuxCostTypesAndNetAsset';
import {PensionSchemeTotalCosts} from '../../../core/models/PensionSchemeTotalCosts';
import {Utils} from '../../../utils/Utils';
import merge from 'deepmerge';
import {UnitTypeFilter} from '../../../filters/unit-type/unit-type-filter';
import {CostTypeFilter} from '../../../filters/cost-type/cost-type-filter';

@Component({
  selector: 'app-auxiliary-cost',
  templateUrl: './auxiliary-cost.component.html',
  styleUrls: ['./auxiliary-cost.component.scss']
})
export class AuxCostComponent implements OnInit {

  scheme: PensionScheme;

  filteredRows = [];

  columns = [];

  selectedCostTypes = [];

  selectedUnit = 'gbp';

  totalCosts: PensionSchemeTotalCosts;

  auxiliaryCostAndTypes: AuxCostAndType[];

  auxiliaryCostTypesAndNetAssets: AuxCostTypesAndNetAsset[];

  filteredBreakdownData: any;

  filteredComparisionData: any;

  totalFilteredAmount: string;

  private breakdownData: any;

  private comparisionData: any;

  private rows = [];

  private breakdownFilters: Array<Function> = [];

  private comparisionFilters: Array<Function> = [];

  private tableFilters: Array<Function> = [];

  constructor(private route: ActivatedRoute, private schemeService: SchemeService,
              private unitTypeFilter: UnitTypeFilter, private costTypeFilter: CostTypeFilter) {
    this.columns = [
      {name: 'Category', prop: 'category'},
      {name: 'Amount', prop: 'amount'},
      {name: 'Amount (Formatted)', prop: 'formattedAmount'},
    ];

    // Cost Type Filters
    this.breakdownFilters.push(costTypeFilter.forBreakdown);
    this.tableFilters.push(costTypeFilter.forTable);
    this.comparisionFilters.push(costTypeFilter.forAuxComparision);

    // Unit Type Filters
    this.breakdownFilters.push(unitTypeFilter.forBreakdown);
    this.comparisionFilters.push(unitTypeFilter.forAuxComparision);
    this.tableFilters.push(unitTypeFilter.forTable);
  }

  ngOnInit() {
    this.route.parent.data.subscribe((data: { pensionScheme: PensionScheme }) => {
      this.scheme = data.pensionScheme;
      this.schemeService.getTotalCosts(this.scheme.id).then((tc: PensionSchemeTotalCosts) => {
        this.totalCosts = tc;
      })
        .then(this.renderBreakdownChart.bind(this));
    });
  }

  public get pageLoaded() {
    return this.scheme && this.filteredBreakdownData && this.filteredComparisionData && this.filteredRows;
  }

  public renderBreakdownChart() {
    return this.schemeService.getAuxCostAndType(this.scheme.id).then((auxiliaryCostAndTypes: AuxCostAndType[]) => {
      this.auxiliaryCostAndTypes = auxiliaryCostAndTypes;
      this.breakdownData = {
        labels: this.auxiliaryCostAndTypes.map(auxiliaryCostAndType => auxiliaryCostAndType.type),
        original: merge({data: [...this.auxiliaryCostAndTypes]}, {}).data,
        datasets: [{
          data: [...this.auxiliaryCostAndTypes.map(auxiliaryCostAndType => auxiliaryCostAndType.amount)],
          formatted: [...this.auxiliaryCostAndTypes.map(auxiliaryCostAndType => auxiliaryCostAndType.amount)],
          backgroundColor: ['#114D64', 'orange', 'dodgerblue', '#176C89', '#8E8E80'],
        }],
      };
    })
      .then(this.filterBreakdownData.bind(this))
      .then(this.renderBreakdownTable.bind(this))
      .then(this.renderUniverseComparisionChart.bind(this));
  }

  public renderBreakdownTable() {
    this.rows = this.auxiliaryCostAndTypes.map(cost => {
      return {
        category: cost.type,
        amount: cost.amount,
        formattedAmount: Utils.wholeNum(cost.amount),
      };
    });
    this.filterBreakdownTable();
  }

  public renderUniverseComparisionChart() {
    this.schemeService.getUniverseAuxCostTypeAndNetAssets(this.scheme.id)
      .then((auxiliaryCostTypesAndNetAssets: AuxCostTypesAndNetAsset[]) => {
        this.auxiliaryCostTypesAndNetAssets = auxiliaryCostTypesAndNetAssets;
        const auxiliaryCostTypesAndNetAsset = new AuxCostTypesAndNetAsset();
        auxiliaryCostTypesAndNetAsset.net_assets = this.totalCosts.net_asset_amount;
        auxiliaryCostTypesAndNetAsset.auxCostAndTypes = this.auxiliaryCostAndTypes;
        auxiliaryCostTypesAndNetAssets.unshift(auxiliaryCostTypesAndNetAsset);

        function generateTitle(costTypes: AuxCostAndType[]) {
          const costTypeTotals = costTypes.map(c => c.amount);
          const costTotal = costTypeTotals.reduce((t, c) => t + c);
          const mv = costTotal;
          return 'Auxiliary Service Costs: ' + Utils.humanize(mv);
        }

        function generateLabel(costTypes: AuxCostAndType[]) {
          return costTypes.map(c => `${c.type}: ${Utils.humanize(c.amount)}`);
        }

        const datasets = this.auxiliaryCostTypesAndNetAssets.map((costTypesAndNetAsset) => {
          const total_auxiliary_costs = costTypesAndNetAsset.auxCostAndTypes
            .map(cost => cost.amount)
            .reduce((total, cost) => total + cost);
          const net_assets = costTypesAndNetAsset.net_assets;

          return {
            title: generateTitle(costTypesAndNetAsset.auxCostAndTypes),
            label: generateLabel(costTypesAndNetAsset.auxCostAndTypes),
            footer: 'Net Assets: ' + Utils.humanize(costTypesAndNetAsset.net_assets),
            original: merge({...costTypesAndNetAsset}, {}),
            data: [{
              x: net_assets,
              y: total_auxiliary_costs,
            }]
          };
        });

        datasets[0]['backgroundColor'] = '#176C89';

        this.comparisionData = {
          datasets: [...datasets],
        };
      })
      .then(this.filterComparisionData.bind(this));
  }

  get totalCost(): string {
    return Utils.humanize(this.totalCosts.total_cost);
  }

  get auxiliaryCost(): string {
    return Utils.humanize(this.totalCosts.investment_auxiliary_cost);
  }

  get auxiliaryCostBPS(): any {
    return Math.round(
      (this.totalCosts.investment_auxiliary_cost / this.totalCosts.net_asset_amount) * 10000
    );
  }

  filterByCostType(event) {
    this.selectedCostTypes = event.value;
    this.filterBreakdownData();
    this.filterComparisionData();
    this.filterBreakdownTable();
  }

  filterByUnitType(event) {
    this.selectedUnit = event.value;
    this.filterBreakdownData();
    this.filterComparisionData();
    this.filterBreakdownTable();
  }

  private filterBreakdownData() {
    const data = merge({...this.breakdownData}, {});
    const net_asset_amount = this.totalCosts.net_asset_amount ? this.totalCosts.net_asset_amount : 0;

    this.filteredBreakdownData = this.breakdownFilters.reduce((d, filter) => filter(d, {
      net_assets: net_asset_amount,
      selectedUnit: this.selectedUnit,
      selectedCostTypes: this.selectedCostTypes,
    }), data);
  }

  private filterComparisionData() {
    const data = merge({...this.comparisionData}, {});

    this.filteredComparisionData = this.comparisionFilters.reduce((d, filter) => filter(d, {
      label: 'Auxiliary Costs',
      selectedUnit: this.selectedUnit,
      selectedCostTypes: this.selectedCostTypes,
    }), data);
  }

  private filterBreakdownTable() {
    const data = merge({data: [...this.rows]}, {});
    const net_asset_amount = this.totalCosts.net_asset_amount ? this.totalCosts.net_asset_amount : 0;

    this.filteredRows = this.tableFilters.reduce((d, filter) => filter(d, {
      net_assets: net_asset_amount,
      selectedUnit: this.selectedUnit,
      selectedCostTypes: this.selectedCostTypes,
    }), data.data);

    this.calculateTotalFilteredAmount();
  }

  private calculateTotalFilteredAmount() {
    const total = this.filteredRows.reduce((sum, r) => {
      return sum + r.amount;
    }, 0);
    this.totalFilteredAmount = Utils.labelForUnit(
      Math.round(total),
      this.selectedUnit,
      Utils.defaultFormatForUnit(this.selectedUnit));
  }

}
