import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PensionScheme} from '../../../core/models/PensionScheme';
import {SchemeService} from '../../../core/services/scheme.service';
import {PensionSchemeTotalCosts} from '../../../core/models/PensionSchemeTotalCosts';
import {UniverseTotalCostAndNetAsset} from '../../../core/models/UniverseTotalCostAndNetAsset';

import {Utils} from '../../../utils/Utils';
import merge from 'deepmerge';
import {UnitTypeFilter} from '../../../filters/unit-type/unit-type-filter';

import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from '../../../store/state/IAppState';
import { ADD_PENSION_SCHEME_TOTAL_COST, ADD_UNIVERSE_TOTAL_COSTS_NET_ASSET } from '../../../store/actions';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  options: any;

  scheme: PensionScheme;

  totalCosts: PensionSchemeTotalCosts = null;

  selectedUnit = 'gbp';

  filteredBreakdownData: any;

  filteredComparisionData: any;

  private breakdownFilters: Array<Function> = [];

  private comparisionFilters: Array<Function> = [];

  private comparisionData: any;

  private breakdownData: any;

  constructor(private route: ActivatedRoute, private schemeService: SchemeService, private unitTypeFilter: UnitTypeFilter,
    private ngRedux: NgRedux<IAppState>) {
    this.breakdownFilters.push(unitTypeFilter.forBreakdown);
    this.comparisionFilters.push(unitTypeFilter.forComparision);
  }

  ngOnInit() {
    this.route.parent.data.subscribe((data: { pensionScheme: PensionScheme }) => {
      this.scheme = data.pensionScheme;
      this.schemeService.getTotalCosts(this.scheme.id)
        .then(this.renderBreakdownChart.bind(this))
        .then(this.filterBreakdownData.bind(this))
        .then(this.renderUniverseComparisionChart.bind(this));


        this.schemeService.getTotalCosts(this.scheme.id)
        .then(res => {
          const total_costs = {pensionSchemeTotalCosts: res};
          this.ngRedux.dispatch({type: ADD_PENSION_SCHEME_TOTAL_COST, total_costs: total_costs});
        });
    });
  }

  public get pageLoaded() {
    return this.scheme && this.filteredBreakdownData && this.filteredComparisionData;
  }

  get investmentCost(): string {
    return Utils.humanize(this.totalCosts.investment_cost);
  }

  get totalCost(): string {
    return Utils.humanize(this.totalCosts.total_cost);
  }

  get adminCost(): string {
    return Utils.humanize(this.totalCosts.admin_cost);
  }

  filterByUnitType(event) {
    this.selectedUnit = event.value;
    this.filterBreakdownData();
    this.filterComparisionData();
  }

  private generateLabel(totalCosts: number, netAssets: number) {
    return [`Total Costs: ${Utils.humanize(totalCosts)}`, `Net Assets: ${Utils.humanize(netAssets)}`];
  }

  private renderBreakdownChart(tc: PensionSchemeTotalCosts) {
    this.totalCosts = tc;
    this.breakdownData = {
      labels: ['Administrative Costs', 'Investment Costs'],
      datasets: [{
        data: [this.totalCosts.admin_cost, this.totalCosts.investment_cost],
        formatted: [Utils.humanize(this.totalCosts.admin_cost), Utils.humanize(this.totalCosts.investment_cost)],
        backgroundColor: ['#114D64', '#176C89'],
      }],
    };
  }

  private renderUniverseComparisionChart() {
    this.schemeService
      .getUniverseTotalCostAndNetAssets(this.scheme.id)
      .then((universeTotalCostAndAssets: UniverseTotalCostAndNetAsset[]) => {
        const pensionSchemeCostAsset = new UniverseTotalCostAndNetAsset();

        const universe_total_costs_net_assets = {universeTotalCostAndNetAsset: pensionSchemeCostAsset};
        this.ngRedux.dispatch({type: ADD_UNIVERSE_TOTAL_COSTS_NET_ASSET, universe_total_costs_net_assets: universe_total_costs_net_assets});

        pensionSchemeCostAsset.total_costs = this.totalCosts.total_cost;
        pensionSchemeCostAsset.net_assets = this.totalCosts.net_asset_amount;
        universeTotalCostAndAssets.unshift(pensionSchemeCostAsset);

        const datasets = universeTotalCostAndAssets.map((universeTotalCostAndAsset) => {
          const total_costs = universeTotalCostAndAsset.total_costs;
          const net_assets = universeTotalCostAndAsset.net_assets;

          return {
            label: this.generateLabel(universeTotalCostAndAsset.total_costs, universeTotalCostAndAsset.net_assets),
            pointRadius: 8,
            pointHoverRadius: 10,
            data: [{
              x: net_assets,
              y: total_costs,
            }],
          };
        });

        datasets[0]['backgroundColor'] = '#176C89';

        const filteredDataset = datasets.filter(ds => ds.data[0].y < ds.data[0].x);

        this.comparisionData = {
          datasets: [...filteredDataset],
        };
      })
      .then(this.filterComparisionData.bind(this));
  }

  private filterBreakdownData() {
    const data = merge({...this.breakdownData}, {});
    const net_asset_amount = this.totalCosts.net_asset_amount ? this.totalCosts.net_asset_amount : 0;

    this.filteredBreakdownData = this.breakdownFilters.reduce((d, filter) => filter(d, {
      net_assets: net_asset_amount,
      selectedUnit: this.selectedUnit
    }), data);
  }

  private filterComparisionData() {
    const data = merge({...this.comparisionData}, {});

    this.filteredComparisionData = this.comparisionFilters.reduce((d, filter) => filter(d, {
      selectedUnit: this.selectedUnit
    }), data);
  }
}
