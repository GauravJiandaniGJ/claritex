import {Component, OnInit, ViewChild} from '@angular/core';
import {PensionScheme} from '../../../core/models/PensionScheme';
import {ActivatedRoute} from '@angular/router';
import {SchemeService} from '../../../core/services/scheme.service';
import {PensionSchemeTotalCosts} from '../../../core/models/PensionSchemeTotalCosts';
import merge from 'deepmerge';
import {UnitTypeFilter} from '../../../filters/unit-type/unit-type-filter';
import {MandateService} from '../../../core/services/mandate.service';
import {Utils} from '../../../utils/Utils';
import {Cost} from '../../../core/models/Cost';
import {ChartTypesService} from '../../../core/services/chart-types.service';

class PerformanceFilters {
  managerFilter: Function = (o) => o;
  mandateFilter: Function = (o) => o;
  categoryFilter: Function = (o) => o;
  typeFilter: Function = (o) => o;
  assetFilter: Function = (o) => o;
}

@Component({
  selector: 'app-performance',
  templateUrl: './performance.component.html',
})
export class PerformanceComponent implements OnInit {

  public static GROSS_PERFORMANCE_ID = '1';

  public static CLIENT_PERFORMANCE_ID = '4';

  public static GROSS_BENCHMARK_ID = '5';

  public static CURRENT_QUARTER_ID = '1';

  public static ONE_YEAR_ID = '2';

  public static THREE_YEAR_ID = '3';

  public static FIVE_YEAR_ID = '4';

  @ViewChild('portfolioTable') private portfolioTable: any;

  scheme: PensionScheme;

  public filteredBreakdownData: any;

  breakdownTypes = [];

  filteredRows = [];

  selectedUnit = 'gbp';

  selectedCostTypes = [];

  private rows = [];

  public performance: Cost;

  private totalCosts: PensionSchemeTotalCosts;

  private breakdownFilters: Array<Function> = [];

  public filteredBreakdownTargetData: any;

  data = {
    labels: [
      'January',
      'February',
      'March',
      'April',
      'May'
    ],
    datasets: [{
      label: 'Range of Foo',
      backgroundColor: ['#114D64', 'orange', 'dodgerblue', '#176C89', '#8E8E80', '#C2A878', '#30343F'],
      data: [150, -50, 310, 240, 500]
    }]
  };

  public relativeBreakdownData: any;

  constructor(private route: ActivatedRoute, private schemeService: SchemeService,
              private mandateService: MandateService, private unitTypeFilter: UnitTypeFilter,
              private chartService: ChartTypesService) {
    //
  }

  ngOnInit() {
    this.breakdownTypes = this.chartService.performanceBreakdownChartTypes();
    this.route.parent.data.subscribe((data: { pensionScheme: PensionScheme }) => {
      this.scheme = data.pensionScheme;

      this.schemeService.getTotalCosts(this.scheme.id).then((tc: PensionSchemeTotalCosts) => {
        this.totalCosts = tc;
      })
        .then(this.fetchData.bind(this));
    });
  }

  public get pageLoaded() {
    return true;
  }

  private fetchData() {
    this.fetchPerformance().then(performance => {
      this.performance = performance;
      this.renderTable();
      this.renderBreakdownChart();
    });
  }

  public renderTable() {
    this.rows = this.performance.breakdown.map(portfolio => {
      return {
        id: portfolio.id,
        name: portfolio.name,
        total: portfolio.amount,
        portfolio: portfolio,
        formattedTotal: Utils.wholeNum(portfolio.amount),
      };
    });

    const data = merge({data: [...this.rows]}, {});

    this.filteredRows = data.data;
  }

  public renderBreakdownChart() {
    const data = merge({rows: this.rows}, {});

    this.filteredBreakdownData = {
      labels: [...data.rows.map(portfolio => portfolio.name)],
      original: merge({data: data.rows}, {}).data,
      datasets: [{
        label: 'Absolute Performance',
        data: [
          ...data.rows
            .map(portfolio => this
              .getPerformance(portfolio.portfolio, PerformanceComponent.CURRENT_QUARTER_ID, PerformanceComponent.GROSS_PERFORMANCE_ID)
            )
        ],
        formatted: [
          ...data.rows
            .map(portfolio => this
              .getPerformanceFormatted(
                portfolio.portfolio,
                PerformanceComponent.CURRENT_QUARTER_ID,
                PerformanceComponent.GROSS_PERFORMANCE_ID
              )
            )
        ],
        backgroundColor: ['#114D64', 'orange', 'dodgerblue', '#176C89', '#8E8E80', '#C2A878', '#30343F'],
      }],
    };

    this.relativeBreakdownData = {
      labels: this.rows.map(portfolio => portfolio.name),
      original: merge({data: this.rows}, {}).data,
      datasets: [{
        label: 'Relative Performance',
        data: [
          ...this.rows
            .map(portfolio => Number(this
              .relativeBenchmarkPerformance(portfolio.portfolio, false))
            )
        ],
        formatted: [
          ...this.rows
            .map(portfolio => this
              .relativeBenchmarkPerformance(portfolio.portfolio)
            )
        ],
        backgroundColor: ['#114D64', 'orange', 'dodgerblue', '#176C89', '#8E8E80', '#C2A878', '#30343F'],
      }],
    };
  }

  private fetchPerformance() {
    return this.schemeService.getPerformance(this.scheme.id);
  }

  formatTotal(total, parentTotal) {
    if (this.selectedUnit === 'percent') {
      return this.formatTotalPercent(total, parentTotal);
    }

    return Utils.labelForUnit(total, this.selectedUnit, '0,0');
  }

  formatTotalPercent(total, parentTotal) {
    return Utils.labelForUnit((total / parentTotal) * 100, 'percent', '0,0.[0]');
  }

  formatRelative(current, parent) {
    const currentTotal = (current.amount / parent.amount) * 100;
    const parentTotal = (current.info.target_amount / parent.info.target_amount) * 100;

    return Utils.labelForUnit(currentTotal - parentTotal, 'percent', '0,0.[0]');
  }

  toggleDetail(event) {
    //
  }

  togglePortfolioRow(row) {
    this.portfolioTable.rowDetail.toggleExpandRow(row);
  }

  public getPerformance(portfolio, period, type) {
    const perf = portfolio.info[type];
    return perf.info[period].amount;
  }

  getPerformanceFormatted(portfolio, period, type) {
    const amt = this.getPerformance(portfolio, period, type);
    return Utils.labelForUnit(amt, 'percent', '0.[00]');
  }

  grossPerformance(portfolio, period) {
    return this.getPerformanceFormatted(portfolio, period, PerformanceComponent.GROSS_PERFORMANCE_ID);
  }

  grossPerformanceForCurrentQuarter(portfolio) {
    return this.grossPerformance(portfolio, PerformanceComponent.CURRENT_QUARTER_ID);
  }

  grossPerformanceForOneYear(portfolio) {
    return this.grossPerformance(portfolio, PerformanceComponent.ONE_YEAR_ID);
  }

  grossPerformanceForThreeYears(portfolio) {
    return this.grossPerformance(portfolio, PerformanceComponent.THREE_YEAR_ID);
  }

  grossPerformanceForFiveYears(portfolio) {
    return this.grossPerformance(portfolio, PerformanceComponent.FIVE_YEAR_ID);
  }

  grossBenchmarkPerformance(portfolio) {
    return this.getPerformanceFormatted(portfolio, PerformanceComponent.ONE_YEAR_ID, PerformanceComponent.GROSS_BENCHMARK_ID);
  }

  relativeBenchmarkPerformance(portfolio, formatted = true): any {
    const gross_perf = this.getPerformance(portfolio, PerformanceComponent.CURRENT_QUARTER_ID, PerformanceComponent.GROSS_PERFORMANCE_ID);
    const gross_bench = this.getPerformance(portfolio, PerformanceComponent.ONE_YEAR_ID, PerformanceComponent.GROSS_BENCHMARK_ID);

    const amt = gross_perf - gross_bench;

    if (!formatted) {
      return amt;
    }
    return Utils.labelForUnit(amt, 'percent', '0.[00]');
  }

}
