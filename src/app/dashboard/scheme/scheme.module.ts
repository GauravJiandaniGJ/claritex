import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SchemeRoutingModule} from './scheme-routing.module';
import {SchemeComponent} from './scheme.component';
import {ThemeModule} from '../../theme/theme.module';
import {IndexComponent} from './index/index.component';
import {ChartModule} from 'angular2-chartjs';
import {AdminCostComponent} from './admin-cost/admin-cost.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {AuxCostComponent} from './auxiliary-cost/auxiliary-cost.component';
import {ChartComponent} from '../../components/chart/chart.component';
import {BreakdownChartComponent} from '../../charts/breakdown/breakdown-chart.component';
import {WaterfallChartComponent} from '../../charts/waterfall/waterfall-chart.component';
import {UniverseComparisionChartComponent} from '../../charts/universe-comparision/universe-comparision-chart.component';
import {UnitTypeFilterComponent} from '../../filters/unit-type/unit-type-filter.component';
import {CostTypeFilterComponent} from '../../filters/cost-type/cost-type-filter.component';
import {UnitTypeFilter} from '../../filters/unit-type/unit-type-filter';
import {StickyDirective} from '../../directives/sticky/sticky.directive';
import {ManagementCostComponent} from './management-cost/management-cost.component';
import {CostTypeFilter} from '../../filters/cost-type/cost-type-filter';
import {CostCategoryFilter} from '../../filters/cost-category/cost-category-filter';
import {CostCategoryFilterComponent} from '../../filters/cost-category/cost-category-filter.component';
import {AssetManagerFilterComponent} from '../../filters/asset-manager/asset-manager-filter.component';
import {AssetManagerFilter} from '../../filters/asset-manager/asset-manager-filter';
import {MandateFilter} from '../../filters/mandate/mandate-filter';
import {MandateFilterComponent} from '../../filters/mandate/mandate-filter.component';
import {AssetClassFilterComponent} from '../../filters/asset-class/asset-class-filter.component';
import {AssetClassFilter} from '../../filters/asset-class/asset-class-filter';
import {IntegratedReportComponent} from './performance/integrated-report.component';

import {DeductCostComponent} from './deduct-cost/deduct_cost.component';
import {AssetAllocationComponent} from './asset-allocation/asset-allocation.component';
import {PerformanceComponent} from './performances/performance.component';

import {PerformanceChartComponent} from '../../charts/performance/performance-chart.component';
import {InvestmentCostComponent} from './investment-cost/investment-cost.component';
import {ManagementManagerComponent} from './management-cost/views/manager.component';
import {ManagementMandateComponent} from './management-cost/views/mandate.component';
import {RecurringCostComponent} from './recurring-cost/recurring-cost.component';
import {RecurringCostTypeComponent} from './recurring-cost/views/cost-type.component';
import {RecurringManagerComponent} from './recurring-cost/views/manager.component';
import {RecurringMandateComponent} from './recurring-cost/views/mandate.component';
import {ManagementCostTypeComponent} from './management-cost/views/cost-type.component';

@NgModule({
  imports: [
    CommonModule,
    SchemeRoutingModule,
    ThemeModule,
    ChartModule,
    NgxDatatableModule,
  ],
  providers: [
    UnitTypeFilter,
    CostTypeFilter,
    CostCategoryFilter,
    AssetManagerFilter,
    MandateFilter,
    AssetClassFilter,
  ],
  declarations: [
    SchemeComponent,
    IndexComponent,
    AdminCostComponent,
    AuxCostComponent,
    ManagementCostComponent,
    IntegratedReportComponent,
    DeductCostComponent, AssetAllocationComponent, PerformanceComponent,
    ChartComponent,
    BreakdownChartComponent,
    WaterfallChartComponent,
    UniverseComparisionChartComponent,
    PerformanceChartComponent,
    InvestmentCostComponent,
    UnitTypeFilterComponent,
    CostTypeFilterComponent,
    CostCategoryFilterComponent,
    AssetManagerFilterComponent,
    MandateFilterComponent,
    AssetClassFilterComponent,
    ManagementManagerComponent,
    ManagementMandateComponent,
    ManagementCostComponent,
    ManagementCostTypeComponent,
    RecurringCostComponent,
    StickyDirective,
    RecurringCostTypeComponent,
    RecurringManagerComponent,
    RecurringMandateComponent,
  ],
})
export class SchemeModule {
}
