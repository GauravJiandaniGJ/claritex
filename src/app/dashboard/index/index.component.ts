import {Component, OnInit} from '@angular/core';
import {SchemeService} from '../../core/services/scheme.service';
import {PensionScheme} from '../../core/models/PensionScheme';
import {PensionSchemeMandate} from '../../core/models/PensionSchemeMandate';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MandateService} from '../../core/services/mandate.service';

import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from '../../store/state/IAppState';
import { ADD_ALL_PENSION_SCHEME } from '../../store/actions';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  loading: boolean;

  error: boolean;


  pensionSchemes: PensionScheme[] = [];

  limitPerPage = 4;

  page = 1;

  selected: PensionScheme;

  columns = [
    {name: 'Mandate', prop: 'name'},
    {name: 'Compliance Check', prop: 'compliance'},
  ];

  mandates: PensionSchemeMandate[] = [];

  private allSchemes: PensionScheme[] = [];

  hasMore: boolean;

  get hasPensionSchemes() {
    return !this.loading && !this.error && this.pensionSchemes.length;
  }

  // @select() pensionScheme;

  constructor(private schemeService: SchemeService,
              private mandateService: MandateService,
              private modalService: NgbModal,
              private ngRedux: NgRedux<IAppState>) {
  }

  ngOnInit() {
    this.fetchPensionSchemes();
  }

  private fetchPensionSchemes() {
    this.loading = true;
    this.schemeService.all()
      .then(pensionSchemeCollection => {
        this.allSchemes = pensionSchemeCollection.all();
        this.loadMore();
      })
      .then(() => this.loading = false)
      .catch(error => {this.error = true; });
  }

  loadMore() {
    this.pensionSchemes = this.pensionSchemes.concat(this.paginate(this.allSchemes, this.limitPerPage, this.page++));
    this.hasMore = this.pensionSchemes.length < this.allSchemes.length;
    const scheme = {pensionScheme: this.pensionSchemes};
    this.ngRedux.dispatch({type: ADD_ALL_PENSION_SCHEME, scheme: scheme});
  }

  paginate(array, page_size, page_number) {
    --page_number;
    return array.slice(page_number * page_size, (page_number + 1) * page_size);
  }

  viewCompliance(id: any, content) {
    this.selected = this.allSchemes.find(s => s.id === id);
    this.mandateService.allByScheme(id).then(m => this.mandates = m).then(() => {
      this.mandates.forEach(m => {
        m.template = 'IDWG';

        const start = new Date(2017, 7, 1);
        const end = new Date(2018, 1, 1);

        m.created_at = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
      });
      this.modalService.open(content, {size: 'lg'});
    });
  }
}
