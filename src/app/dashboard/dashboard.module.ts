import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DashboardRoutingModule} from './dashboard-routing.module';
import {DashboardComponent} from './dashboard.component';
import {ThemeModule} from '../theme/theme.module';
import {IndexComponent} from './index/index.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ThemeModule,
    NgxDatatableModule,
  ],
  declarations: [DashboardComponent, IndexComponent],
})
export class DashboardModule {
}
