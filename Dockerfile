# Stage `node`. Based on Node.js. Compile Angular and Build dist artifacts.
FROM node:8.9-alpine as node
WORKDIR /app
COPY package.json /app/
RUN npm install
COPY ./ /app/
ARG env=prod
RUN npm run build -- --prod --environment $env

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.13 as app
COPY --from=node /app/dist/ /usr/share/nginx/html
COPY ./docker/nginx/app.conf /etc/nginx/conf.d/default.conf
